﻿namespace Ubicomp.Gem.BookshelfClient.Test
{
    using Ubicomp.Gem.BookshelfServer;
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            var gestureProducer = new GestureProducer();
            gestureProducer.Connect("localhost", 3999);

            ////var gestureConsumer = new GestureConsumer();
            ////gestureConsumer.Connect("localhost", 3999);
            ////gestureConsumer.GestureReceived += Receive;

            ////var bookshelfClient = new BookshelfClient(AgentCapabilities.BookViewer);
            ////bookshelfClient.Connect("localhost", 3999);
            ////var list = bookshelfClient.GetAvailableBooks();

            while (true)
            {
                gestureProducer.BroadcastGesture(new GestureInfo(GestureTypes.Point, 0, 0, 0));
                Console.ReadKey();
            }
        }

        private static void Receive(object sender, GestureEvent evt)
        {
            Console.ReadKey();
        }
    }
}
