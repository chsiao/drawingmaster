﻿namespace SketchGame.Server
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using System.Xml.Serialization;
    
    public class SketchServer
    {
        #region Fields

        private static readonly object padlock = new object();

        private readonly IList<SketchAgent> connectedClients = new List<SketchAgent>();

        private TcpListener tcpListener;

        private Thread listenerThread;


        #endregion

        #region Constructors & Destructors

        public SketchServer(int port)
        {            
            this.tcpListener = new TcpListener(IPAddress.Any, port);
        }

        #endregion

        #region Public Methods

        public void Start()
        {
            this.listenerThread = new Thread(new ThreadStart(this.ListenForClients));
            this.listenerThread.Start();
        }

        #endregion

        #region Private methods
        

        private static void Send(NetworkStream stream, Command toSend)
        {
            var bytes = Encoding.ASCII.GetBytes(toSend.ToJson());
            //Console.WriteLine("Sending " + bytes.Length + " bytes.");
            //Console.WriteLine("Message to send: " + toSend.ToJson());
            stream.Write(bytes, 0, bytes.Length);
            stream.Flush();
        }

        private static string Receive(NetworkStream stream)
        {
            int bytesRead = 0;
            var byteBuffer = new byte[4096];
            var responseBuffer = new StringBuilder();

            //Console.WriteLine("Waiting on a request...");
            
            do
            {
                bytesRead = stream.Read(byteBuffer, 0, byteBuffer.Length);
                string readstring = Encoding.ASCII.GetString(byteBuffer, 0, bytesRead);
                //Console.WriteLine("Message received: " + readstring);
                //Console.WriteLine("");
                responseBuffer.Append(readstring);
                //Console.WriteLine("Read " + bytesRead + " bytes");
            }
            while (stream.DataAvailable && bytesRead > 0);

            //System.Diagnostics.Debug.WriteLine("Message received: " + responseBuffer.ToString());
            //System.Diagnostics.Debug.WriteLine("");
            return responseBuffer.ToString();
        }

        private void HandleAgent(object agent)
        {
            var sketchAgent = agent as SketchAgent;
            if (agent != null)
            {
                Console.WriteLine("Agent created, building the processor thread.");
                var processing = true;
                while (processing)
                {
                    var request = "";
                    try
                    {                        
                        request = Receive(sketchAgent.Client.GetStream());
                        object command = string.IsNullOrEmpty(request) ? null : request.FromJsonToType<Command>();
                        
                        // the json object is invalid, it needs further processing
                        if (command == null)
                        {
                            command = string.IsNullOrEmpty(request) ? null : request.FromJsonToTypeList<Command>();

                            if (command != null)
                            {
                                string leftover = ((Tuple<List<Command>, string>)command).Item2;
                                command = ((Tuple<List<Command>, string>)command).Item1;
                            }
                        }


                        if (command != null)
                        {
                            if(command is Command)
                                ProcessCommand((Command)command, sketchAgent);
                            else if (command is List<Command>)
                            {
                                foreach (var singCmd in (List<Command>)command)
                                    ProcessCommand(singCmd, sketchAgent);
                            }
                        }
                        
                    }
                    catch (Exception e)
                    {
                        // try again
                        try
                        {
                            object command = string.IsNullOrEmpty(request) ? null : request.FromJsonToTypeList<Command>();
                           
                            if (command != null)
                            {
                                string leftover = ((Tuple<List<Command>, string>)command).Item2;
                                command = ((Tuple<List<Command>, string>)command).Item1;

                                if (command is Command)
                                    ProcessCommand((Command)command, sketchAgent);
                                else if (command is List<Command>)
                                {
                                    foreach (var singCmd in (List<Command>)command)
                                        ProcessCommand(singCmd, sketchAgent);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Console.WriteLine("Exception caught during agent processing: " + ex.ToString());
                            //processing = false; // don't remove the agent for now. have more tolerances!!
                        }                       
                    }
                }

                lock (padlock)
                {
                    Console.WriteLine("Removing dead agent");
                    // Remove the agent, we're probably done with it.
                    this.connectedClients.Remove(sketchAgent);
                }
            }
        }

        private void ProcessCommand(Command request, SketchAgent callee)
        {
            string calleeAddr = ((IPEndPoint)callee.Client.Client.RemoteEndPoint).Address.ToString();

            if (request != null)
            {
                //Console.WriteLine("Processing command " + request.Name);
                switch (request.Name)
                {
                    case CommandNames.Register:
                        lock (padlock)
                        {
                            var payload = (long?)request.Payload;
                            if (payload.HasValue && Enum.IsDefined(typeof(AgentCapabilities), (int)payload.Value))
                            {
                                callee.Capabilities = (AgentCapabilities)(int)payload.Value;
                                Send(callee.Client.GetStream(), Command.ResponseFromRequest(request, true));
                            }
                        }

                        break;
                    case CommandNames.StartSketch:
                        lock (padlock)
                        {
                            var payload = (long?)request.Payload;
                            if (payload.HasValue && Enum.IsDefined(typeof(AgentCapabilities), (int)payload.Value))
                            {
                                callee.Capabilities = (AgentCapabilities)(int)payload.Value;
                                Send(callee.Client.GetStream(), Command.ResponseFromRequest(request, true));
                            }
                            else
                            {
                                Send(callee.Client.GetStream(), Command.ResponseFromRequest(request, false));
                            }
                        }


                        break;
                    case CommandNames.EndSketch:                        

                        //Send(callee.Client.GetStream(), Command.ResponseFromRequest(request, list));

                        break;
                    case CommandNames.StartStroke:

                        foreach (var agent in this.connectedClients)
                        {
                            string agentAddr = ((IPEndPoint)agent.Client.Client.RemoteEndPoint).Address.ToString();


                            if (agent.Capabilities == AgentCapabilities.SketchReceive && calleeAddr != agentAddr)
                            {
                                Send(agent.Client.GetStream(), request);
                            }
                        }

                        break;
                    case CommandNames.EndStroke:

                        foreach (var agent in this.connectedClients)
                        {
                            string agentAddr = ((IPEndPoint)agent.Client.Client.RemoteEndPoint).Address.ToString();

                            if (agent.Capabilities == AgentCapabilities.SketchReceive && agentAddr != calleeAddr)
                            {
                                Send(agent.Client.GetStream(), request);
                            }
                        }

                        break;

                    case CommandNames.SendPacketPoint:

                        foreach (var agent in this.connectedClients)
                        {
                            string agentAddr = ((IPEndPoint)agent.Client.Client.RemoteEndPoint).Address.ToString();

                            if (agent.Capabilities == AgentCapabilities.SketchReceive && agentAddr != calleeAddr)
                            {
                                Send(agent.Client.GetStream(), request);
                            }
                        }

                        break;
                    case CommandNames.Spying:
                        foreach (var agent in this.connectedClients)
                        {
                            string agentAddr = ((IPEndPoint)agent.Client.Client.RemoteEndPoint).Address.ToString();

                            if (agent.Capabilities == AgentCapabilities.SketchCompetetion && agentAddr != calleeAddr)
                            {
                                Send(agent.Client.GetStream(), request);
                            }
                        }

                        break;
                }
            }
        }

        private void ListenForClients()
        {
            Console.WriteLine("Listening for clients...");
            this.tcpListener.Start();

            while (true)
            {
                var client = this.tcpListener.AcceptTcpClient();

                Console.WriteLine("Got a new client!");
                var agent = new SketchAgent(AgentCapabilities.Unknown, client);

                // Lock before manipulating the list of connected clients.
                lock (padlock)
                {
                    this.connectedClients.Add(agent);
                }

                var clientThread = new Thread(new ParameterizedThreadStart(this.HandleAgent));
                clientThread.Start(agent);



                // broacast the prepare message for all the players that are ready!!
                if (this.connectedClients.Count >= 4)
                {
                    Command cmd = new Command(CommandNames.Prepare, CommandTypes.Response);

                    foreach (var respondingAgent in this.connectedClients)
                    {
                        if (respondingAgent.Capabilities == AgentCapabilities.SketchReceive)
                            Send(respondingAgent.Client.GetStream(), cmd);
                    }
                }
            }
        }
  
        #endregion
    }

    public class DataContainer
    {
        List<Command> Container{get;set;}
    }
}
