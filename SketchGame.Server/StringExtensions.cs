﻿namespace SketchGame.Server
{
    using System.Collections.Generic;
    using System;
    using System.Diagnostics;
    using Newtonsoft.Json;

    public static class StringExtensions
    {
        #region Extension Methods

        public static T FromJsonToType<T>(this string value)
        {
            try
            {
                var serializerSettings = new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.All
                };

                if (value[0] == ',') value = value.Remove(0);
                if (value[value.Length - 1] == ',') value = value.Remove(value.Length-1);

                return JsonConvert.DeserializeObject<T>(value, serializerSettings);
            }
            catch (Exception e)
            {
                Trace.WriteLine(
                    "Exception thrown while attempting to deserialize the JSON " +
                     value +
                    " to the type " + 
                    typeof(T).FullName + 
                    ". Details: " +
                    e.ToString());
            }

            return default(T);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns>left: the returned list object; right: the left over string value</returns>
        public static Tuple<List<T>,string> FromJsonToTypeList<T>(this string value)
        {
            var aObjs = new List<T>();
            var leftover = "";

            var serializerSettings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All
            };

            if (value[0] == ',') value = value.Remove(0);
            else if (value[0] != '{') 
            { // this is not a valid json, need to remove the first part
                int indexInvalidHead = value.IndexOf("},{");

                if (indexInvalidHead == -1) return null;

                value = value.Remove(0, indexInvalidHead + 2);
            }


            if (value[value.Length - 1] == ',') value = value.Remove(value.Length - 1);
            else // need to search the previous comma and split it into two strings
            {
                int indexLeftOver = value.LastIndexOf("},{");
                leftover = value.Remove(0, indexLeftOver + 1);
                value = value.Remove(indexLeftOver + 1);
            }

            value = "[" + value + "]";

            aObjs = JsonConvert.DeserializeObject<List<T>>(value, serializerSettings);


            return new Tuple<List<T>,string>(aObjs, leftover);
        }

        #endregion
    }
}
