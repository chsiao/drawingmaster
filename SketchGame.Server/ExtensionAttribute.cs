﻿namespace System.Runtime.CompilerServices
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Hack that makes extension methods work in frameworks lower than 4.0
    /// </summary>
    public class ExtensionAttribute : Attribute
    {
    }
}
