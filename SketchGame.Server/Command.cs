﻿namespace SketchGame.Server
{
    using System.Collections.Generic;
    
    public enum CommandNames
    {
        Register,
        Prepare,
        ShowBackground,
        DisappearBackground,
        StartSketch,
        EndSketch,
        SendPacketPoint, // the packet points in a stroke, including the attributes of the packet point, such as location, timestamps and pressures
        StartStroke, // the stroke attributes, include the brush and the color
        EndStroke, // end the stroke
        Spying  // the location and the area of spy                
    }

    public enum CommandTypes
    {
        Request,
        Response,
        Send
    }

    public class Command
    {
        #region Constructors & Destructors

        private Command()
        {
        }

        public Command(CommandNames name, CommandTypes type)
        {
            this.Name = name;
            this.Type = type;
            this.Payload = null;
        }

        #endregion

        #region Properties

        public CommandNames Name
        {
            get;
            private set;
        }

        public CommandTypes Type
        {
            get;
            private set;
        }

        public object Payload
        {
            get;
            set;
        }

        #endregion

        #region Public Methods

        public static Command ResponseFromRequest(Command request, object payload)
        {
            if (request.Type == CommandTypes.Request)
            {
                return new Command(request.Name, CommandTypes.Response)
                {
                    Payload = payload,
                    Type = CommandTypes.Response
                };
            }
            else
            {
                // Cant make a response from a response.
                return null;
            }
        }

        public static Command CreateRequest(CommandNames name, object payload = null)
        {
            return new Command()
            {
                Name = name,
                Type = CommandTypes.Request,
                Payload = payload
            };
        }
        #endregion        

    }

    public class PacketPointCommand : Command
    {
        private int preesure, size, strokeID;
        // Preparing the space for packet points and time stamps
        public PacketPointCommand(CommandNames name, CommandTypes type)
            : base(name, type)
        {
            
        }

        /// <summary>
        /// get or set the location of this packet point
        /// </summary>
        public int X
        {
            set;
            get;
        }

        public int Y
        {
            set;
            get;
        }
        
        /// <summary>
        /// get or set the timestamp of this packet point
        /// </summary>
        public long TimeStamp
        {
            set;
            get;
        }        

        public int Pressure
        {
            get
            {
                return this.preesure;
            }
            set
            {
                this.preesure = value;
            }
        }

        public int Size
        {
            get
            {
                return this.size;
            }
            set
            {
                this.size = value;
            }
        }

        public int StrokeID
        {
            get
            {
                return this.strokeID;
            }
            set
            {
                this.strokeID = value;
            }
        }

        public void SetTimeLong(System.DateTime datetime)
        {
            System.DateTime JanFirst1970 = new System.DateTime(1970, 1, 1);
            this.TimeStamp = (long)((datetime - JanFirst1970).TotalMilliseconds + 0.5);
        }

    }

}
