﻿namespace SketchGame.Server
{
    internal class Program
    {
        #region Public Methods

        public static void Main(string[] args)
        {
            SketchServer server = new SketchServer(3999);
            server.Start();
        }

        #endregion
    }
}
