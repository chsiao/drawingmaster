﻿namespace SketchGame.Server
{
    using System;
    using System.Net.Sockets;

    public enum AgentCapabilities : int
    {
        Unknown,
        SketchCompetetion,
        CoSketchSend,
        SketchReceive,
        SingleMode
    }

    public class SketchAgent
    {
        #region Constructors & Destructors

        public SketchAgent(AgentCapabilities capabilities, TcpClient client)
        {
            this.Capabilities = capabilities;
            this.Client = client;
        }

        #endregion

        #region Properties

        public AgentCapabilities Capabilities
        {
            get;
            set;
        }

        public TcpClient Client
        {
            get;
            private set;
        }

        #endregion
    }
}
