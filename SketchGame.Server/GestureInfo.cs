﻿namespace Ubicomp.Gem.BookshelfServer
{
    public enum GestureTypes
    {
        Point,
        Grab,
        FlipLeft,
        FlipRight
    }

    public class GestureInfo
    {
        #region Constructors & Destructors

        public GestureInfo(GestureTypes type, int x, int y, int z)
        {
            this.Type = type;
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        #endregion

        #region Properties

        public GestureTypes Type
        {
            get;
            private set;
        }

        public int X
        {
            get;
            private set;
        }

        public int Y
        {
            get;
            private set;
        }

        public int Z
        {
            get;
            private set;
        }

        #endregion
    }
}
