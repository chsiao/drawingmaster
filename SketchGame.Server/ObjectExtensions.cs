﻿namespace SketchGame.Server
{
    using System;
    using System.Diagnostics;
    using Newtonsoft.Json;

    public static class ObjectExtensions
    {
        #region Extension Methods

        public static string ToJson(this object obj)
        {
            try
            {
                var serializerSettings = new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.All
                };

                return JsonConvert.SerializeObject(obj, Formatting.None, serializerSettings) + ",";
            }
            catch (Exception e)
            {
                Trace.WriteLine(
                    "Exception thrown while attempting to serialize type " +
                    obj.GetType().FullName +
                    ". Details: " +
                    e.ToString());
            }

            return null;
        }

        #endregion
    }
}
