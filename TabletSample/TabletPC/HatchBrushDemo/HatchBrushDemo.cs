//--------------------------------------------------
// HatchBrushDemo.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//--------------------------------------------------
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

class HatchBrushDemo: GdiPlusRendererDemo
{
    public new static void Main()
    {
        Application.Run(new HatchBrushDemo());
    }
    public HatchBrushDemo()
    {
        Text = "Hatch Brush Demo";
        Width *= 2;
        Height *= 2;

        // Create a Pen based on a HatchBrush 
        HatchBrush brsh = new HatchBrush(HatchStyle.HorizontalBrick, 
                                         Color.White, Color.Black);
        gdiplus.Pen = new Pen(brsh, 1270);
        gdiplus.Pen.StartCap = gdiplus.Pen.EndCap = LineCap.Round;

        // Make a 5-pointed star with a radius of 3 inches.
        PointF[] aptf = new PointF[5];
        float fRadius = 3 * 2540f;

        for (int i = 0; i < 5; i++)
            aptf[i] = new PointF(fRadius * (1 + (float) Math.Sin(i * Math.PI / 1.25)),
                                 fRadius * (1 - (float) Math.Cos(i * Math.PI / 1.25)));

        // Create a path from the points.
        GraphicsPath path = new GraphicsPath();
        path.AddLines(aptf);
        path.CloseFigure();

        // Use that star for clipping.
        gdiplus.Clip = new Region(path);
    }
}
