//---------------------------------------------------
// WhirligigPlugin.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//---------------------------------------------------
using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;
using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

class WhirligigPlugin: IStylusSyncPlugin, IStylusAsyncPlugin
{
    const int iRadius = 2540 / 4;		// 1/4 inch
    const double dAngleIncr = 0.02;	    // degrees per linear hi-metric unit
    Point ptStylus1;
    double dAngle;

    // Required property
    public DataInterestMask DataInterest
    {
        get
        {
            return DataInterestMask.StylusDown | 
                   DataInterestMask.Packets |
                   DataInterestMask.StylusUp;
        }
    }

    // Implemented methods
    public void StylusDown(RealTimeStylus sender, StylusDownData data)
    {
        ptStylus1 = new Point(data[0], data[1]);
        dAngle = 0;
        data[0] += iRadius;     // Alter the point slightly
    }
    public void Packets(RealTimeStylus sender, PacketsData data) 
    {
        Generate(data);
    }
    public void StylusUp(RealTimeStylus sender, StylusUpData data)
    {
        Generate(data);
    }
    public void Generate(StylusDataBase data)
    {
        // Create an ArrayList for storing points.
        ArrayList arrlst = new ArrayList();

        // Loop through the packets
        for (int iPacket = 0; iPacket < data.Count; iPacket += data.PacketPropertyCount)
        {
            Point ptStylus2 = new Point(data[iPacket + 0], data[iPacket + 1]);

            // Calculate the linear distance the stylus moved.
            double dDistance = Math.Sqrt(Math.Pow(ptStylus2.X - ptStylus1.X, 2) +
                                         Math.Pow(ptStylus2.Y - ptStylus1.Y, 2));

            // dDistance can be 1 (if moved one unit horizontally or vertically),
            //      or 1.414 (if moved one unit diagonally), or something larger.
            int iDivisions = (int) Math.Ceiling(dDistance);

			// This loop generates the new points.	
            for (int i = 1; i <= iDivisions; i++)
            {
                // Sub-divide the distance between the last stylus point and the new
                Point pt = new Point((i * ptStylus2.X + (iDivisions - i) * ptStylus1.X) / iDivisions,
                                     (i * ptStylus2.Y + (iDivisions - i) * ptStylus1.Y) / iDivisions);

                dAngle += dDistance * dAngleIncr / iDivisions;
			
		        // Now offset and rotate the point
                pt.X += (int) (iRadius * Math.Cos(dAngle));
                pt.Y += (int) (iRadius * Math.Sin(dAngle));

                arrlst.Add(pt);
            }
            ptStylus1 = ptStylus2;
        }
        
        // Create a new array of integers for the new points.
        int[] arr = new int[data.PacketPropertyCount * arrlst.Count];

        // Fill up that array with the points and other packet data.
        for (int i = 0, j = 0; i < arr.Length; i += data.PacketPropertyCount)
        {
            Point pt = (Point) arrlst[j++];
            arr[i] = pt.X;
            arr[i + 1] = pt.Y;

            for (int k = 2; k < data.PacketPropertyCount; k++)
                arr[i + k] = data[k];
        }
        // Set the data for the next plug-in in the chain.
        data.SetData(arr);
    }

    // Methods with empty bodies
    public void CustomStylusDataAdded(RealTimeStylus sender, CustomStylusData data) {}
    public void Error(RealTimeStylus sender, ErrorData data) {}
    public void InAirPackets(RealTimeStylus sender, InAirPacketsData data) {}
    public void RealTimeStylusDisabled(RealTimeStylus sender, RealTimeStylusDisabledData data) {}
    public void RealTimeStylusEnabled(RealTimeStylus sender, RealTimeStylusEnabledData data) {}
    public void StylusButtonDown(RealTimeStylus sender, StylusButtonDownData data) {}
    public void StylusButtonUp(RealTimeStylus sender, StylusButtonUpData data) {}
    public void StylusInRange(RealTimeStylus sender, StylusInRangeData data) {}
    public void StylusOutOfRange(RealTimeStylus sender, StylusOutOfRangeData data) {}
    public void SystemGesture(RealTimeStylus sender, SystemGestureData data) {}
    public void TabletAdded(RealTimeStylus sender, TabletAddedData data) {}
    public void TabletRemoved(RealTimeStylus sender, TabletRemovedData data) {}
}
