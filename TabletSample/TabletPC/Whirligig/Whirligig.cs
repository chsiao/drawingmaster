//---------------------------------------------
// Whirligig.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//---------------------------------------------
using Microsoft.StylusInput;
using System;
using System.Drawing;
using System.Windows.Forms;

class Whirligig: Form
{
    DynamicRenderer dynaren;

    public static void Main()
    {
        Application.Run(new Whirligig());
    }
    public Whirligig()
    {
        Text = "Whirligig";

        RealTimeStylus rts = new RealTimeStylus(this);
        rts.SyncPluginCollection.Add(new WhirligigPlugin());

        dynaren = new DynamicRenderer(this);
        dynaren.Enabled = true;
        dynaren.EnableDataCache = true;

        rts.SyncPluginCollection.Add(dynaren);
        rts.Enabled = true;
    }
    protected override void OnPaint(PaintEventArgs args)
    {
        Rectangle rectSave = dynaren.ClipRectangle;
        dynaren.ClipRectangle = args.ClipRectangle;
        dynaren.Refresh();
        dynaren.ClipRectangle = rectSave;
    }
}
