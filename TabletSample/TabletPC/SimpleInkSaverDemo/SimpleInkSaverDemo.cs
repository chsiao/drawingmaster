//------------------------------------------------------
// SimpleInkSaverDemo.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//------------------------------------------------------
using Microsoft.Ink;
using Microsoft.StylusInput;
using System;
using System.Drawing;
using System.Windows.Forms;

class SimpleInkSaverDemo: Form
{
    Label lblOutput;
    SimpleInkSaverPlugin inkplugin;

    public static void Main()
    {
        Application.Run(new SimpleInkSaverDemo());
    }
    public SimpleInkSaverDemo()
    {
        Text = "Simple Ink-Saver Demo";

        // Put a panel, label, and button on the client area.
        Panel pnl = new Panel();
        pnl.Parent = this;
        pnl.Dock = DockStyle.Fill;
        pnl.Paint += new PaintEventHandler(PanelOnPaint);

        lblOutput = new Label();
        lblOutput.Parent = this;
        lblOutput.BorderStyle = BorderStyle.FixedSingle;
        lblOutput.Dock = DockStyle.Top;

        Button btn = new Button();
        btn.Parent = this;
        btn.Dock = DockStyle.Top;
        btn.Text = "Convert Ink to Text";
        btn.Click += new EventHandler(ButtonOnClick);

        // RealTimeStylus now uses Panel for input.
        RealTimeStylus rts = new RealTimeStylus(pnl);
        rts.SyncPluginCollection.Add(new SimpleRendererPlugin(pnl));

        inkplugin = new SimpleInkSaverPlugin();
        rts.AsyncPluginCollection.Add(inkplugin);
        rts.Enabled = true;
    }
    void PanelOnPaint(object objSrc, PaintEventArgs args)
    {
        Graphics grfx = args.Graphics;
        grfx.PageUnit = GraphicsUnit.Millimeter;
        grfx.PageScale = 0.01f;
        grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

        foreach (Stroke stk in inkplugin.Ink.Strokes)
            grfx.DrawLines(SystemPens.ControlText, stk.GetPoints());
    }
    void ButtonOnClick(object objSrc, EventArgs args)
    {
        Recognizers recos = new Recognizers();

        if (recos.Count > 0 && inkplugin.Ink.Strokes.Count > 0)
        {
            RecognizerContext recoContext = new RecognizerContext();
            recoContext.Strokes = inkplugin.Ink.Strokes;

            RecognitionStatus recoStatus;
            RecognitionResult recoResult = recoContext.Recognize(out recoStatus);

            lblOutput.Text = recoResult.TopString;
        }
        else
        {
            lblOutput.Text = "Sorry, no recognizers installed.";
        }
    }
}
