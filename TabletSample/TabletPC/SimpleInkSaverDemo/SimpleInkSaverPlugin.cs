//--------------------------------------------------------
// SimpleInkSaverPlugin.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//--------------------------------------------------------
using Microsoft.Ink;
using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;
using System;
using System.Collections;
using System.Drawing;

class SimpleInkSaverPlugin : IStylusAsyncPlugin, IStylusSyncPlugin
{
    Ink ink = new Ink();
    ArrayList arrlstStroke;

    // Property
    public Ink Ink
    {
        get { return ink; }
    }

    // Required property
    public DataInterestMask DataInterest
    {
        get
        {
            return DataInterestMask.StylusDown | 
                   DataInterestMask.Packets |
                   DataInterestMask.StylusUp;
        }
    }

    // Implemented methods
    public void StylusDown(RealTimeStylus sender, StylusDownData data)
    {
        arrlstStroke = new ArrayList();
        arrlstStroke.Add(new Point(data[0], data[1]));
    }
    public void Packets(RealTimeStylus sender, PacketsData data)
    {
        for (int i = 0; i < data.Count; i += data.PacketPropertyCount)
            arrlstStroke.Add(new Point(data[i], data[i + 1]));
    }
    public void StylusUp(RealTimeStylus sender,  StylusUpData data)
    {
        arrlstStroke.Add(new Point(data[0], data[1]));
        Point[] apt = (Point[]) arrlstStroke.ToArray(typeof(Point));
        ink.CreateStroke(apt);
    }

    // Methods with empty bodies
    public void CustomStylusDataAdded(RealTimeStylus sender, CustomStylusData data) {}
    public void Error(RealTimeStylus sender, ErrorData data) {}
    public void InAirPackets(RealTimeStylus sender, InAirPacketsData data) {}
    public void RealTimeStylusDisabled(RealTimeStylus sender, RealTimeStylusDisabledData data) {}
    public void RealTimeStylusEnabled(RealTimeStylus sender, RealTimeStylusEnabledData data) {}
    public void StylusButtonDown(RealTimeStylus sender, StylusButtonDownData data) {}
    public void StylusButtonUp(RealTimeStylus sender, StylusButtonUpData data) {}
    public void StylusInRange(RealTimeStylus sender, StylusInRangeData data) {}
    public void StylusOutOfRange(RealTimeStylus sender, StylusOutOfRangeData data) {}
    public void SystemGesture(RealTimeStylus sender, SystemGestureData data) {}
    public void TabletAdded(RealTimeStylus sender, TabletAddedData data) {}
    public void TabletRemoved(RealTimeStylus sender, TabletRemovedData data) {}
}
