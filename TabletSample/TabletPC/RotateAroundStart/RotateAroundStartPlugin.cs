//-----------------------------------------------------------
// RotateAroundStartPlugin.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//-----------------------------------------------------------
using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

class RotateAroundStartPlugin : IStylusSyncPlugin, IStylusAsyncPlugin
{
    float fAngle;
    Matrix xform;

    // Constructor
    public RotateAroundStartPlugin(float fAngle)
    {
        this.fAngle = fAngle;
    }

    // Required property
    public DataInterestMask DataInterest
    {
        get
        {
            return 
                DataInterestMask.StylusDown |
                DataInterestMask.Packets |
                DataInterestMask.StylusUp;
        }
    }

    // Implemented methods.
    public void StylusDown(RealTimeStylus sender,  StylusDownData data)
    {
        // Create matrix for rotation around point.
        xform = new Matrix();
        xform.RotateAt(fAngle, new Point(data[0], data[1]));
    }
    public void Packets(RealTimeStylus sender,  PacketsData data)
    {
        TransformPoints(data);
    }
    public void StylusUp(RealTimeStylus sender,  StylusUpData data)
    {
        TransformPoints(data);
    }
    void TransformPoints(StylusDataBase data)
    {
        Point[] apt = new Point[data.Count / data.PacketPropertyCount];

        // Get the points.
        for (int i = 0; i < data.Count; i += data.PacketPropertyCount)
            apt[i] = new Point(data[i], data[i + 1]);

        // Transform the points.
        xform.TransformPoints(apt);

        // Set the points.
        for (int i = 0; i < data.Count; i += data.PacketPropertyCount)
        {
            data[i] = apt[i].X;
            data[i + 1] = apt[i].Y;
        }
    }

    // Methods with empty bodies
    public void Error(RealTimeStylus sender, ErrorData data) {}
    public void RealTimeStylusDisabled(RealTimeStylus sender, RealTimeStylusDisabledData data) {}
    public void RealTimeStylusEnabled(RealTimeStylus sender, RealTimeStylusEnabledData data){}
    public void StylusOutOfRange(RealTimeStylus sender, StylusOutOfRangeData data) {}
    public void StylusInRange(RealTimeStylus sender, StylusInRangeData data) {}
    public void StylusButtonDown(RealTimeStylus sender, StylusButtonDownData data) {}
    public void StylusButtonUp(RealTimeStylus sender, StylusButtonUpData data) {}
    public void CustomStylusDataAdded(RealTimeStylus sender, CustomStylusData data){}
    public void SystemGesture(RealTimeStylus sender, SystemGestureData data){}
    public void InAirPackets(RealTimeStylus sender, InAirPacketsData data){}
    public void TabletAdded(RealTimeStylus sender, TabletAddedData data){}
    public void TabletRemoved(RealTimeStylus sender, TabletRemovedData data) {}
}
