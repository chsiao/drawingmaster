//-----------------------------------------------------
// RotateAroundStart.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//-----------------------------------------------------
using Microsoft.StylusInput;
using System;
using System.Drawing;
using System.Windows.Forms;

class RotateAroundStart : Form
{
    const int iAngle = 30;      // should be divisor or 360

    public static void Main()
    {
        Application.Run(new RotateAroundStart());
    }
    public RotateAroundStart()
    {
        Text = "Rotate Around Start";

        RealTimeStylus rts = new RealTimeStylus(this);

        for (int i = 0; i < 360 / iAngle; i++)
        {
            rts.SyncPluginCollection.Add(new RotateAroundStartPlugin(iAngle));
            DynamicRenderer dynaren = new DynamicRenderer(this);
            dynaren.Enabled = true;
            rts.SyncPluginCollection.Add(dynaren);
        }
        rts.Enabled = true;
    }
    protected override void OnDoubleClick(EventArgs args)
    {
        Invalidate();
    }
}
