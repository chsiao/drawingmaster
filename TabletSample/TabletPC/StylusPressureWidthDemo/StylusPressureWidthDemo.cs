//-----------------------------------------------------------
// StylusPressureWidthDemo.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//-----------------------------------------------------------
using Microsoft.StylusInput;
using System;
using System.Drawing;
using System.Windows.Forms;

class StylusPressureWidthDemo: Form
{
    public static void Main()
    {
        Application.Run(new StylusPressureWidthDemo());
    }
    public StylusPressureWidthDemo()
    {
        Text = "Stylus Pressure Width Demo";

        RealTimeStylus rts = new RealTimeStylus(this);
        rts.SyncPluginCollection.Add(new StylusPressureWidthPlugin(this));
        rts.Enabled = true;
    }
}
