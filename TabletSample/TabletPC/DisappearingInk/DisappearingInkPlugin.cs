//--------------------------------------------------------
// DisapperingInkPlugin.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//--------------------------------------------------------
using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;
using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

class DisappearingInkPlugin: IStylusSyncPlugin
{
    long msecAllGone = 5 * 1000; 
    Color clrPen = Color.Red;
    float fPenWidth = 200;
    Control ctrl;
    ArrayList alPoints;
    ArrayList alStrokes = new ArrayList();

    public DisappearingInkPlugin(Control ctrl)
    {
        this.ctrl = ctrl;

        Timer tmr = new Timer();
        tmr.Tick += new EventHandler(TimerOnTick);
        tmr.Interval = 250;
        tmr.Enabled = true;
    }

    void TimerOnTick(object objSrc, EventArgs args)
    {
        Graphics grfx = ctrl.CreateGraphics();
        grfx.PageUnit = GraphicsUnit.Millimeter;
        grfx.PageScale = 0.01f;

        // Loop through all strokes
        for (int j = 0; j < alStrokes.Count; j++)
        {
            ArrayList alPoints = (ArrayList) alStrokes[j];
            TimeStampedPoint tsp1 = (TimeStampedPoint) alPoints[0];

            // Loop through all points in the stroke
            for (int i = 1; i < alPoints.Count; i++)
            {
                TimeStampedPoint tsp2 = (TimeStampedPoint) alPoints[i];
                long msec = tsp2.Age;

                if (msec < msecAllGone)
                {
                    // Calculate the color based on the age of the line
                    double dFactor = (double) (msecAllGone - msec) / msecAllGone;

                    Color clr = Color.FromArgb(
                        (int) (dFactor * clrPen.R + (1 - dFactor) * ctrl.BackColor.R),
                        (int) (dFactor * clrPen.G + (1 - dFactor) * ctrl.BackColor.G),
                        (int) (dFactor * clrPen.B + (1 - dFactor) * ctrl.BackColor.B));

                    // Draw the line
                    Pen pn = new Pen(clr, fPenWidth);
                    pn.StartCap = pn.EndCap = LineCap.Round;
                    grfx.DrawLine(pn, tsp1.Point, tsp2.Point);
                    pn.Dispose();
                }
                tsp1 = tsp2;
            }
        }
        grfx.Dispose();
    }

    // Required property
    public DataInterestMask DataInterest
    { 
        get 
        { 
            return DataInterestMask.Packets | 
                   DataInterestMask.StylusDown | 
                   DataInterestMask.StylusUp; }
    }

    // Implemented methods
    public void StylusDown(RealTimeStylus sender, StylusDownData data) 
    {
        Point pt = new Point(data[0], data[1]);
        alPoints = new ArrayList();
        alPoints.Add(new TimeStampedPoint(pt));
        alStrokes.Add(alPoints);
    }

    public void Packets(RealTimeStylus sender, PacketsData data) 
    {
        RenderData(data);
    }
    public void StylusUp(RealTimeStylus sender, StylusUpData data) 
    {
        RenderData(data);
    }
    void RenderData(StylusDataBase data)
    {
        Graphics grfx = ctrl.CreateGraphics();
        grfx.PageUnit = GraphicsUnit.Millimeter;
        grfx.PageScale = 0.01f;

        for (int i = 0; i < data.Count; i += data.PacketPropertyCount)
        {
            // Save the new point
            Point pt1 = ((TimeStampedPoint) alPoints[alPoints.Count - 1]).Point;
            Point pt2 = new Point(data[i + 0], data[i + 1]);
            alPoints.Add(new TimeStampedPoint(pt2));

            // Draw the line
            Pen pn = new Pen(clrPen, fPenWidth);
            pn.StartCap = pn.EndCap = LineCap.Round;
            grfx.DrawLine(pn, pt1, pt2);
            pn.Dispose();
        }
        grfx.Dispose();
    }
    
    // Unimplemented methods
    public void CustomStylusDataAdded(RealTimeStylus sender, CustomStylusData data) {}
    public void Error(RealTimeStylus sender, ErrorData data) {}
    public void InAirPackets(RealTimeStylus sender, InAirPacketsData data) {}
    public void RealTimeStylusDisabled(RealTimeStylus sender, RealTimeStylusDisabledData data) {}
    public void RealTimeStylusEnabled(RealTimeStylus sender, RealTimeStylusEnabledData data) {}
    public void StylusButtonDown(RealTimeStylus sender, StylusButtonDownData data) {}
    public void StylusButtonUp(RealTimeStylus sender, StylusButtonUpData data) {}
    public void StylusInRange(RealTimeStylus sender, StylusInRangeData data) {}
    public void StylusOutOfRange(RealTimeStylus sender, StylusOutOfRangeData data) {}
    public void SystemGesture(RealTimeStylus sender, SystemGestureData data) {}
    public void TabletAdded(RealTimeStylus sender, TabletAddedData data) {}
    public void TabletRemoved(RealTimeStylus sender, TabletRemovedData data) {}
}