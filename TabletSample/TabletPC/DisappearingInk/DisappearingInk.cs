//--------------------------------------------------
// DisapperingInk.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//--------------------------------------------------
using Microsoft.StylusInput;
using System;
using System.Drawing;
using System.Windows.Forms;

class DisappearingInk: Form
{
    public static void Main()
    {
        Application.Run(new DisappearingInk());
    }
    public DisappearingInk()
    {
        Text = "Disappearing Ink";
        BackColor = Color.White;

        RealTimeStylus rts = new RealTimeStylus(this);
        rts.SyncPluginCollection.Add(new DisappearingInkPlugin(this));
        rts.Enabled = true;
    }
}