//----------------------------------------------------
// TimeStampedPoint.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//----------------------------------------------------
using System;
using System.Drawing;

class TimeStampedPoint
{
    Point pt;
    DateTime dt;

    public TimeStampedPoint(Point pt)
    {
        this.pt = pt;
        dt = DateTime.Now;
    }
    public Point Point
    {
        get { return pt; }
    }
    public long Age
    {
        get { return (long) (DateTime.Now - dt).TotalMilliseconds; }
    }
}
