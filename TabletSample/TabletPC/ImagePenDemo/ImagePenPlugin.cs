//--------------------------------------------------
// ImagePenPlugin.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005.
//--------------------------------------------------
using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

class ImagePenPlugin: IStylusSyncPlugin, IStylusAsyncPlugin
{
    float fWidth = 36;
    Image img;
    Pen pn;
    Point ptLast;
    Control ctrl;

    // Constructor
    public ImagePenPlugin(Control ctrl)
    {
        this.ctrl = ctrl;
    }

    // Public properties
    public float WidthInPoints
    {
        set { fWidth = value; }
        get { return fWidth; }
    }
    public Image Image
    {
        set { img = value; }
        get { return img; }
    }

    // Required property
    public DataInterestMask DataInterest
    { 
        get 
        { 
            return DataInterestMask.StylusDown |
                   DataInterestMask.Packets | 
                   DataInterestMask.StylusUp; 
        }
    }

    // Implemented methods
    public void StylusDown(RealTimeStylus sender, StylusDownData data) 
    {
        Graphics grfx = ctrl.CreateGraphics();
        ptLast = new Point((int) (grfx.DpiX * data[0] / 2540 + 0.5), 
                           (int) (grfx.DpiY * data[1] / 2540 + 0.5));

        TextureBrush tb = new TextureBrush(Image, WrapMode.Tile);
        float fPenWidth = grfx.DpiX * WidthInPoints / 72;
        pn = new Pen(tb, fPenWidth);
        pn.StartCap = pn.EndCap = LineCap.Round;
        grfx.Dispose();
    }
    public void Packets(RealTimeStylus sender, PacketsData data) 
    {
        RenderData(data);
    }
    public void StylusUp(RealTimeStylus sender, StylusUpData data)
    {
        RenderData(data);
        pn.Dispose();
    }
    void RenderData(StylusDataBase data)
    {
        Graphics grfx = ctrl.CreateGraphics();

        for (int i = 0; i < data.Count; i += data.PacketPropertyCount)
        {
            Point pt = new Point((int) (grfx.DpiX * data[i + 0] / 2540 + 0.5), 
                                 (int) (grfx.DpiY * data[i + 1] / 2540 + 0.5));
            if (pt != ptLast)
            {
                grfx.DrawLine(pn, ptLast, pt);
                ptLast = pt;
            }
        }
        grfx.Dispose();
    }   
    public void CustomStylusDataAdded(RealTimeStylus sender, CustomStylusData data) {}
    public void Error(RealTimeStylus sender, ErrorData data) {}
    public void InAirPackets(RealTimeStylus sender, InAirPacketsData data) {}
    public void RealTimeStylusDisabled(RealTimeStylus sender, RealTimeStylusDisabledData data) {}
    public void RealTimeStylusEnabled(RealTimeStylus sender, RealTimeStylusEnabledData data) {}
    public void StylusButtonDown(RealTimeStylus sender, StylusButtonDownData data) {}
    public void StylusButtonUp(RealTimeStylus sender, StylusButtonUpData data) {}
    public void StylusInRange(RealTimeStylus sender, StylusInRangeData data) {}
    public void StylusOutOfRange(RealTimeStylus sender, StylusOutOfRangeData data) {}
    public void SystemGesture(RealTimeStylus sender, SystemGestureData data) {}
    public void TabletAdded(RealTimeStylus sender, TabletAddedData data) {}
    public void TabletRemoved(RealTimeStylus sender, TabletRemovedData data) {}
}

