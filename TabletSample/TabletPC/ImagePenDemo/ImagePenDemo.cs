//------------------------------------------------
// ImagePenDemo.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//------------------------------------------------
using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

class ImagePenDemo: Form
{
    public static void Main()
    {
        Application.Run(new ImagePenDemo());
    }
    public ImagePenDemo()
    {
        Text = "Image Pen Demo";

        ImagePenPlugin plugin = new ImagePenPlugin(this);
        RealTimeStylus rts = new RealTimeStylus(this);
        rts.SyncPluginCollection.Add(plugin);
        rts.Enabled = true;

        // Create modeless dialog to change ImagePenPlugin properties
        Form dlg = new Form();
        dlg.Text = "ImagePenPlugin Properties";
        dlg.Owner = this;

        PropertyGrid prop = new PropertyGrid();
        prop.Parent = dlg;
        prop.Dock = DockStyle.Fill;
        prop.SelectedObject = plugin;

        dlg.Show();
    }
}
