//-------------------------------------------------------
// GdiPlusRendererDemo.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//-------------------------------------------------------
using Microsoft.StylusInput;
using System;
using System.Drawing;
using System.Windows.Forms;

class GdiPlusRendererDemo: Form
{
    protected GdiPlusRendererPlugin gdiplus;

    public static void Main()
    {
        Application.Run(new GdiPlusRendererDemo());
    }
    public GdiPlusRendererDemo()
    {
        Text = "Gdi+ Renderer Demonstration";

        gdiplus = new GdiPlusRendererPlugin(this);

        RealTimeStylus rts = new RealTimeStylus(this);
        rts.SyncPluginCollection.Add(gdiplus);
        rts.Enabled = true;

        // Create PropertyGrid control in modeless dialog.
        Form dlg = new Form();
        dlg.Owner = this;
        dlg.Text = "GdiPlusRenderer Pen";
        PropertyGrid propgrid = new PropertyGrid();
        propgrid.Parent = dlg;
        propgrid.Dock = DockStyle.Fill;
        propgrid.SelectedObject = gdiplus.Pen;
        dlg.Show();

    }
}
