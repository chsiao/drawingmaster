//---------------------------------------------------------
// GdiPlusRendererPlugin.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//---------------------------------------------------------
using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

class GdiPlusRendererPlugin: IStylusSyncPlugin
{
    Control ctrl;
    Point ptLast;
    Pen pn;
    Pen pnStroke;
    Region rgn;

    public GdiPlusRendererPlugin(Control ctrl)
    {
        this.ctrl = ctrl;
        pn = new Pen(ctrl.ForeColor, 53);
    }
    public Pen Pen
    {
        get { return pn; }
        set { pn = value; }
    }
    public Region Clip
    {
        get { return rgn; }
        set { rgn = value; }
    }

    public DataInterestMask DataInterest
    {
        get 
        { 
            return DataInterestMask.StylusDown | 
                DataInterestMask.Packets |
                DataInterestMask.StylusUp; 
        }
    }
    public void StylusDown(RealTimeStylus sender, StylusDownData data)
    {
        ptLast = new Point(data[0], data[1]);
        pnStroke = (Pen) Pen.Clone();
    }
    public void Packets(RealTimeStylus sender, PacketsData data)
    {
        RenderData(data);
    }
    public void StylusUp(RealTimeStylus sender, StylusUpData data)
    {
        RenderData(data);
        pnStroke.Dispose();
    }
    void RenderData(StylusDataBase data)
    {
        Graphics grfx = ctrl.CreateGraphics();
        grfx.PageUnit = GraphicsUnit.Millimeter;
        grfx.PageScale = 0.01f;
        grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

        if (Clip != null)
            grfx.Clip = Clip;

        for (int i = 0; i < data.Count; i += data.PacketPropertyCount)
        {
            Point pt = new Point(data[i], data[i + 1]);
            grfx.DrawLine(pn, ptLast, pt);
            ptLast = pt;
        }
        grfx.Dispose();
    }
    public void CustomStylusDataAdded(RealTimeStylus sender, CustomStylusData data) {}
    public void Error(RealTimeStylus sender, ErrorData data) {}
    public void InAirPackets(RealTimeStylus sender, InAirPacketsData data) {}
    public void RealTimeStylusDisabled(RealTimeStylus sender, RealTimeStylusDisabledData data) {}
    public void RealTimeStylusEnabled(RealTimeStylus sender, RealTimeStylusEnabledData data) {}
    public void StylusButtonDown(RealTimeStylus sender, StylusButtonDownData data) {}
    public void StylusButtonUp(RealTimeStylus sender, StylusButtonUpData data) {}
    public void StylusInRange(RealTimeStylus sender, StylusInRangeData data) {}
    public void StylusOutOfRange(RealTimeStylus sender, StylusOutOfRangeData data) {}
    public void SystemGesture(RealTimeStylus sender, SystemGestureData data) {}
    public void TabletAdded(RealTimeStylus sender, TabletAddedData data) {}
    public void TabletRemoved(RealTimeStylus sender, TabletRemovedData data) {}
}
