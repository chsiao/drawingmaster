//-----------------------------------------------------------
// StylusPressureColorDemo.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//-----------------------------------------------------------
using Microsoft.StylusInput;
using System;
using System.Drawing;
using System.Windows.Forms;

class StylusPressureColorDemo: Form
{
    public static void Main()
    {
        Application.Run(new StylusPressureColorDemo());
    }
    public StylusPressureColorDemo()
    {
        Text = "Stylus Pressure Color Demo";

        RealTimeStylus rts = new RealTimeStylus(this);
        rts.SyncPluginCollection.Add(new StylusPressureColorPlugin(this));
        rts.Enabled = true;
    }
}
