//--------------------------------------------------------
// SimpleRendererPlugin.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//--------------------------------------------------------
using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;
using System;
using System.Drawing;
using System.Windows.Forms;

class SimpleRendererPlugin : IStylusAsyncPlugin, IStylusSyncPlugin
{
    Control ctrl;
    Point ptLast;

    // Constructor
    public SimpleRendererPlugin(Control ctrl)
    {
        this.ctrl = ctrl;
    }

    // Required property
    public DataInterestMask DataInterest
    {
        get
        {
            return DataInterestMask.StylusDown | 
                   DataInterestMask.Packets |
                   DataInterestMask.StylusUp;
        }
    }

    // Implemented methods
    public void StylusDown(RealTimeStylus sender, StylusDownData data)
    {
        ptLast = new Point(data[0], data[1]);
    } 
    public void Packets(RealTimeStylus sender, PacketsData data)
    {
        Graphics grfx = ctrl.CreateGraphics();
        grfx.PageUnit = GraphicsUnit.Millimeter;
        grfx.PageScale = 0.01f;
        grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

        for (int i = 0; i < data.Count; i += data.PacketPropertyCount)
        {
            Point pt = new Point(data[i], data[i + 1]);
            grfx.DrawLine(SystemPens.ControlText, ptLast, pt);
            ptLast = pt;
        }
        grfx.Dispose();
    }
    public void StylusUp(RealTimeStylus sender,  StylusUpData data)
    {
        Graphics grfx = ctrl.CreateGraphics();
        grfx.PageUnit = GraphicsUnit.Millimeter;
        grfx.PageScale = 0.01f;
        grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        grfx.DrawLine(SystemPens.ControlText, ptLast, new Point(data[0], data[1]));
        grfx.Dispose();
    }

    // Methods with empty bodies
    public void CustomStylusDataAdded(RealTimeStylus sender, CustomStylusData data) {}
    public void Error(RealTimeStylus sender, ErrorData data) {}
    public void InAirPackets(RealTimeStylus sender, InAirPacketsData data) {}
    public void RealTimeStylusDisabled(RealTimeStylus sender, RealTimeStylusDisabledData data) {}
    public void RealTimeStylusEnabled(RealTimeStylus sender, RealTimeStylusEnabledData data) {}
    public void StylusButtonDown(RealTimeStylus sender, StylusButtonDownData data) {}
    public void StylusButtonUp(RealTimeStylus sender, StylusButtonUpData data) {}
    public void StylusInRange(RealTimeStylus sender, StylusInRangeData data) {}
    public void StylusOutOfRange(RealTimeStylus sender, StylusOutOfRangeData data) {}
    public void SystemGesture(RealTimeStylus sender, SystemGestureData data) {}
    public void TabletAdded(RealTimeStylus sender, TabletAddedData data) {}
    public void TabletRemoved(RealTimeStylus sender, TabletRemovedData data) {}
}
