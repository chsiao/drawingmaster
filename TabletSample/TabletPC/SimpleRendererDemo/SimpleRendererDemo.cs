//------------------------------------------------------
// SimpleRendererDemo.cs, programmed by Charles Petzold
// MSDN Magazine, December 2005
//------------------------------------------------------
using Microsoft.StylusInput;
using System;
using System.Drawing;
using System.Windows.Forms;

class SimpleRendererDemo: Form
{
    public static void Main()
    {
        Application.Run(new SimpleRendererDemo());
    }
    public SimpleRendererDemo()
    {
        Text = "Simple RTS Renderer Demo";

        RealTimeStylus rts = new RealTimeStylus(this);
        rts.SyncPluginCollection.Add(new SimpleRendererPlugin(this));
        rts.Enabled = true;
    }
}
