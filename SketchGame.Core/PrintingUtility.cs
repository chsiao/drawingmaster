﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Ink;


namespace SketchGame.Core
{
    public class PrintingUtility
    {
        InkPicture inkPicture1;
        PrintDialog printDialog1;
        System.Drawing.Printing.PrintDocument pdocument;

        public PrintingUtility(string docName, InkPicture canvas)
        {
            this.inkPicture1 = canvas;
            printDialog1 = new PrintDialog();
            printDialog1.UseEXDialog = true;

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            //printDialog1.AllowPrintToFile = true;            
            pdocument = new System.Drawing.Printing.PrintDocument();
            pdocument.DocumentName = docName;
            pdocument.OriginAtMargins = false;
            pdocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(pdocument_PrintPage);

            printDialog1.Document = pdocument;

            
        }

        public void Print()
        {
            try
            {
                //using (var dlg = new PrintPreviewDialog())
                //{
                //dlg.Document = pdocument;
                //if (dlg.ShowDialog().Equals(DialogResult.OK))
                //{
                if (printDialog1.ShowDialog().Equals(DialogResult.OK))
                    pdocument.Print();
                //}
                //}
            }
            catch (Exception ex)
            {

            }
        }

        void pdocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            //if (printAction != PrintAction.PrintToPreview)
            g.TranslateTransform(-e.PageSettings.HardMarginX, -e.PageSettings.HardMarginY);

            Rectangle printArea = GetBestPrintableArea(e);
            int shortEdge = Math.Min(printArea.Width, printArea.Height);
            printArea.Width = shortEdge;
            printArea.Height = shortEdge;

            if (this.inkPicture1.Image == null)
            {
                e.Cancel = true;
                return;
            }

            Bitmap b2 = new Bitmap(this.inkPicture1.Image);
            Graphics g1 = Graphics.FromImage(b2);
            g1.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            // make a renderer to draw ink on the graphics surface
            Renderer r = new Renderer();
            r.Draw(g1, this.inkPicture1.Ink.Strokes);

            int longEdge = Math.Max(b2.Width, b2.Height);
            Rectangle srcRect = new System.Drawing.Rectangle(0, 0, longEdge, longEdge);
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            e.Graphics.DrawImage(b2, printArea, srcRect, System.Drawing.GraphicsUnit.Pixel);
        }

        public Rectangle GetBestPrintableArea(System.Drawing.Printing.PrintPageEventArgs e)
        {
            RectangleF marginBounds = e.MarginBounds;
            RectangleF printableArea = e.PageSettings.PrintableArea;
            RectangleF pageBounds = e.PageBounds;

            if (e.PageSettings.Landscape)
                printableArea = new RectangleF(printableArea.Y, printableArea.X, printableArea.Height, printableArea.Width);

            Rectangle bestArea = Rectangle.FromLTRB(
                (int)Math.Max(marginBounds.Left, printableArea.Left),
                (int)Math.Max(marginBounds.Top, printableArea.Top),
                (int)Math.Min(marginBounds.Right, printableArea.Right),
                (int)Math.Min(marginBounds.Bottom, printableArea.Bottom)
            );

            float bestMarginX = (float)Math.Max(bestArea.Left, pageBounds.Right - bestArea.Right);
            float bestMarginY = (float)Math.Max(bestArea.Top, pageBounds.Bottom - bestArea.Bottom);

            bestArea = Rectangle.FromLTRB(
                (int)bestMarginX,
                (int)bestMarginY,
                (int)(pageBounds.Right - bestMarginX),
                (int)(pageBounds.Bottom - bestMarginY)
            );

            return bestArea;
        }
    }
}
