﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SketchGame.Core
{
    public class SketchPen
    {
        //public void SketchPen()
        //{

        //}
        public int ColorA { get; set; }
        public int ColorR { get; set; }
        public int ColorG { get; set; }
        public int ColorB { get; set; }

        public string BrushType { get; set; }


    }
}
