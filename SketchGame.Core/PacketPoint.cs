﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SketchGame.Core
{
    public class PacketPoint
    {
        //private Point pkPt = new Point();      // Coordinate for each packet point        
        private long timestamp;
        private int preesure, size, strokeID;
        // Preparing the space for packet points and time stamps
        public PacketPoint()
        {
            //this.pkPt = new Point();
            
        }

        /// <summary>
        /// get or set the location of this packet point
        /// </summary>
        public int X
        {
            set;
            get;
        }

        public int Y
        {
            set;
            get;
        }

        /// <summary>
        /// get or set the timestamp of this packet point
        /// </summary>
        public long TimeStamp
        {
            set
            {
                this.timestamp = value;
            }
            get
            {
                return this.timestamp;
            }
        }        

        public int Pressure
        {
            get
            {
                return this.preesure;
            }
            set
            {
                this.preesure = value;
            }
        }

        public int Size
        {
            get
            {
                return this.size;
            }
            set
            {
                this.size = value;
            }
        }

        public int StrokeID
        {
            get
            {
                return this.strokeID;
            }
            set
            {
                this.strokeID = value;
            }
        }

        public void SetTimeLong(DateTime datetime)
        {
            DateTime JanFirst1970 = new DateTime(1970, 1, 1);
            this.TimeStamp = (long)((datetime - JanFirst1970).TotalMilliseconds + 0.5);
        }
    }
}
