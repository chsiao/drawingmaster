﻿using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Linq;

using SketchGame.Client;
using SketchGame.Server;
using SketchGame.Core;

namespace SketchGame.SketchMaster
{
    public class GdiPlusRendererPlugin : IStylusSyncPlugin
    {
        object drawingLock = new object();
        SketchCanvas ctrl;
        Point ptLast;
        Pen pn;
        Pen currentPen;
        Pen penFromOther;
        bool isOtherStartingNewStroke = false;
        PacketPointCommand prePktCmd;
        Region rgn;
                
        PenStroke myCurStroke;                
        

        #region Events

        public event EventHandler<CommandEvent> PushMessage;

        #endregion

        public GdiPlusRendererPlugin(SketchCanvas ctrl)
        {
            this.ctrl = ctrl;
            this.ctrl.Paint += new PaintEventHandler(ctrl_Paint);
            this.ctrl.MouseMove += new MouseEventHandler(ctrl_MouseMove);
            drawingLock = this.ctrl.drawingLock;

            pn = new Pen(ctrl.ForeColor, 53);
            this.ctrl.strokesFromOther = new Dictionary<string, List<PenStroke>>();
            this.ctrl.strokesFromOther.Add("other", new List<PenStroke>());
                        
            //Point[] points = 
            //{
            //    new Point(-3,-3),
            //    new Point(0,0),
            //    new Point(3,-3)
            //};
            //GraphicsPath path = new GraphicsPath();
            //path.AddLines(points);

            //CustomLineCap cap = new CustomLineCap(null,path);
            //cap.SetStrokeCaps(LineCap.Round, LineCap.Triangle);

            //pn.CustomStartCap = cap;
            //pn.CustomEndCap = cap;

            pn.Brush = new SolidBrush(Color.Black);
        }
        
        
        public Pen Pen
        {
            get { return pn; }
            set { pn = value; }
        }

        public Region Clip
        {
            get { return rgn; }
            set { rgn = value; }
        }

        public DataInterestMask DataInterest
        {
            get
            {
                return DataInterestMask.StylusDown |
                    DataInterestMask.Packets |
                    DataInterestMask.StylusUp;
            }
        }

        public void AddNewStrokeFromOtherAgents(Command cmd)
        {
            lock (drawingLock)
            {
                if (cmd.Payload != null && cmd.Payload is SketchPen)
                {
                    SketchPen otherpen = (SketchPen)cmd.Payload;
                    Color colorOther = Color.FromArgb(otherpen.ColorA, otherpen.ColorR, otherpen.ColorG, otherpen.ColorB);

                    penFromOther = new Pen(colorOther, 53);

                    isOtherStartingNewStroke = true;

                    PenStroke st = new PenStroke();
                    st.Color = colorOther;
                    this.ctrl.strokesFromOther["other"].Add(st);
                }
            }
        }

        public void AddNewPkPointFromOtherAgents(PacketPointCommand pktCmd)
        {
            lock (drawingLock)
            {
                if (isOtherStartingNewStroke)
                {
                    prePktCmd = pktCmd;
                    isOtherStartingNewStroke = false;
                }
                else
                {
                    if (penFromOther == null)
                        penFromOther = new System.Drawing.Pen(Color.Black, 53);
                    RenderStrokesFromOther(pktCmd);
                    prePktCmd = pktCmd;
                }

                AddPkPointFromOther(pktCmd, "other");
            }
        }

        public void EndStrokeFromOtherAgents(Command cmd)
        {
            lock (drawingLock)
            {
                if (cmd.Payload != null && cmd.Payload is PacketPointCommand)
                {
                    if (penFromOther == null)
                        penFromOther = new System.Drawing.Pen(Color.Black, 53);
                    RenderStrokesFromOther((PacketPointCommand)cmd.Payload);
                    prePktCmd = null;
                    isOtherStartingNewStroke = true;

                    AddPkPointFromOther((PacketPointCommand)cmd.Payload, "other");
                }
            }
        }


        public void StylusDown(RealTimeStylus sender, StylusDownData data)
        {
            lock (drawingLock)
            {
                ptLast = new Point(data[0], data[1]);
                currentPen = (Pen)Pen.Clone();

                PenStroke st = new PenStroke();
                st.Color = currentPen.Color;
                ((SketchCanvas)ctrl).myStrokes.Add(st);

                // send the starting point
                PacketPointCommand pkPoint = new PacketPointCommand(CommandNames.SendPacketPoint, CommandTypes.Send);
                pkPoint.X = data[0];
                pkPoint.Y = data[1];
                if (data.PacketPropertyCount > 2)
                    pkPoint.Pressure = data[2];

                pkPoint.SetTimeLong(System.DateTime.Now);

                AddMyPkPoint(pkPoint);

                if (PushMessage != null)
                {
                    // create the entry point for a stroke, which contains the pen object
                    SketchPen pen = new SketchPen();
                    pen.ColorR = currentPen.Color.R;
                    pen.ColorG = currentPen.Color.G;
                    pen.ColorB = currentPen.Color.B;
                    pen.ColorA = currentPen.Color.A;
                    pen.BrushType = "";

                    Command stcm = new Command(CommandNames.StartStroke, CommandTypes.Send);
                    stcm.Payload = pen;
                    CommandEvent cStEvent = new CommandEvent(stcm);
                    PushMessage(this, cStEvent);

                    CommandEvent cevent = new CommandEvent(pkPoint);
                    PushMessage(this, cevent);
                }
            }
        }

        public void Packets(RealTimeStylus sender, PacketsData data)
        {
            lock (drawingLock)
            {
                RenderData(data);

                PacketPointCommand pkPoint = new PacketPointCommand(CommandNames.SendPacketPoint, CommandTypes.Send);
                pkPoint.X = data[0];
                pkPoint.Y = data[1];
                if (data.PacketPropertyCount > 2)
                    pkPoint.Pressure = data[2];

                pkPoint.SetTimeLong(System.DateTime.Now);

                AddMyPkPoint(pkPoint);

                if (PushMessage != null)
                {
                    // send the starting point               
                    CommandEvent cevent = new CommandEvent(pkPoint);
                    PushMessage(this, cevent);
                }
            }
        }

        public void StylusUp(RealTimeStylus sender, StylusUpData data)
        {
            lock (drawingLock)
            {
                RenderData(data);

                // send the starting point
                PacketPointCommand pkPoint = new PacketPointCommand(CommandNames.EndStroke, CommandTypes.Send);
                pkPoint.X = data[0];
                pkPoint.Y = data[1];
                if (data.PacketPropertyCount > 2)
                    pkPoint.Pressure = data[2];

                pkPoint.SetTimeLong(System.DateTime.Now);

                AddMyPkPoint(pkPoint);

                if (PushMessage != null)
                {
                    CommandEvent cevent = new CommandEvent(pkPoint);
                    PushMessage(this, cevent);
                }

                currentPen.Dispose();
            }
        }

        void RenderData(StylusDataBase data)
        {
            Graphics grfx = ctrl.CreateGraphics();
            grfx.PageUnit = GraphicsUnit.Millimeter;
            grfx.PageScale = 0.01f;
            grfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            if (Clip != null)
                grfx.Clip = Clip;

            for (int i = 0; i < data.Count; i += data.PacketPropertyCount)
            {
                Point pt = new Point(data[i], data[i + 1]);

                float fWidth = 53;

                if (data.PacketPropertyCount > 2)
                    fWidth *= 1 + (data[i + 2] - 128f) / 16;                

                pn.Width = fWidth;
                grfx.DrawLine(pn, ptLast, pt);
                ptLast = pt;
            }          

            grfx.Dispose();
        }

        void RenderStrokesFromOther(PacketPointCommand pkCommand)
        {
            if ((ctrl.isSpyMode && SketchCanvas.CurMode == AgentCapabilities.SketchCompetetion) || SketchCanvas.CurMode == AgentCapabilities.CoSketchSend)
            {
                Graphics grfx = ctrl.CreateGraphics();
                grfx.PageUnit = GraphicsUnit.Millimeter;
                grfx.PageScale = 0.01f;
                grfx.SmoothingMode = SmoothingMode.AntiAlias;

                int pressure = pkCommand.Pressure;
                float fWidth = 53;
                if (pressure > 0)
                    fWidth *= 1 + ((float)pressure - 128f) / 16;

                penFromOther.Width = fWidth;
                Point ptLast = new Point(prePktCmd.X, prePktCmd.Y);
                Point pt = new Point(pkCommand.X, pkCommand.Y);
                grfx.DrawLine(penFromOther, ptLast, pt);

                grfx.Dispose();
            }
        }        

        private void AddPkPointFromOther(PacketPointCommand cmd, string client)
        {
            PacketPoint pt = new PacketPoint();
            pt.Pressure = cmd.Pressure;
            pt.X = cmd.X;
            pt.Y = cmd.Y;
            pt.TimeStamp = cmd.TimeStamp;
            this.ctrl.strokesFromOther[client].Last().PacketPoints.Add(pt);
        }

        private void AddMyPkPoint(PacketPointCommand cmd)
        {
            PacketPoint pt = new PacketPoint();
            pt.Pressure = cmd.Pressure;
            pt.X = cmd.X;
            pt.Y = cmd.Y;
            pt.TimeStamp = cmd.TimeStamp;
            ((SketchCanvas)ctrl).myStrokes.Last().PacketPoints.Add(pt);
        }        

        void ctrl_Paint(object sender, PaintEventArgs e)
        {                            
               
        }

        void ctrl_MouseMove(object sender, MouseEventArgs e)
        {
            
        }

        public void CustomStylusDataAdded(RealTimeStylus sender, CustomStylusData data) 
        {
 
        }
        public void Error(RealTimeStylus sender, ErrorData data) 
        { 
        }
        public void InAirPackets(RealTimeStylus sender, InAirPacketsData data) 
        {

        }
        public void RealTimeStylusDisabled(RealTimeStylus sender, RealTimeStylusDisabledData data) 
        {
        }
        public void RealTimeStylusEnabled(RealTimeStylus sender, RealTimeStylusEnabledData data) 
        {
        }
        public void StylusButtonDown(RealTimeStylus sender, StylusButtonDownData data) 
        {
        }
        public void StylusButtonUp(RealTimeStylus sender, StylusButtonUpData data) 
        { 
        }
        public void StylusInRange(RealTimeStylus sender, StylusInRangeData data) 
        {
        }
        public void StylusOutOfRange(RealTimeStylus sender, StylusOutOfRangeData data) 
        {
        }
        public void SystemGesture(RealTimeStylus sender, SystemGestureData data) 
        {
        }
        public void TabletAdded(RealTimeStylus sender, TabletAddedData data) 
        {
        }
        public void TabletRemoved(RealTimeStylus sender, TabletRemovedData data) 
        {
        }
    }

}