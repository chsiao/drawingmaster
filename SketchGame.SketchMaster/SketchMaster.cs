﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Forms;

using Microsoft.StylusInput;
using SketchGame.Core;
using SketchGame.Server;
using SketchGame.Client;


namespace SketchGame.SketchMaster
{
    public enum Colorpick
    {
        Darkblue,
        Green,
        Lightblue,
        Purple,
        Red,
        Yellow,
        Grey,
        White,
        Black
    }

    public enum Brushpick
    {
        plEraser,
        plPencil,
        plPen,
        plMarker,
        plSharpie,
        plBrush
    }

    public enum Level
    {
        easy = 1,
        medium = 2,
        hard = 3,
        complexFigure = 4
    }

    public partial class SketchMaster : Form
    {
        private delegate void StartShowingOriginalDelegate();
        public AgentCapabilities CurMode = AgentCapabilities.SketchCompetetion;
        
        
        private int prepareDuration = 5; // s
        private System.Windows.Forms.Timer preOriginalTimer;

        private int showOriginalDuration = 30; // s
        private System.Windows.Forms.Timer showingOriginalTimer;

        private int sketchDuration = 60; // s
        private System.Windows.Forms.Timer sketchingTimer;

        private System.Timers.Timer spyTime;
        public const int SpyDuration = 30000; //ms

        private bool isShowingOriginal = false;
        private Animation animation;
        protected GdiPlusRendererPlugin gdiplus;
        private SketchReceiver receiver;
        
        private Level level = Level.easy;

        private RealTimeStylus rts;

        public SketchMaster(AgentCapabilities mode, Level level)
        {            
            InitializeComponent();

            this.level = level;
            this.CurMode = mode;
            SketchCanvas.CurMode = this.CurMode;

            if(SketchCanvas.CurMode == AgentCapabilities.SingleMode)
            {
                this.txtBoxServerAddress.Enabled = false;
                this.btConnect.Enabled = false;

                startTheGame();
            }

            switch(level)
            {
                case Level.easy:
                    SketchCanvas.originalPic = Properties.Resources.piceasy;
                    break;
                case Level.medium:
                    SketchCanvas.originalPic = Properties.Resources.picmid;
                    break;
                case Level.hard:
                    SketchCanvas.originalPic = Properties.Resources.pichard;
                    break;
                case Level.complexFigure:
                    SketchCanvas.originalPic = Properties.Resources.ComplexFigure;
                    break;
            }


            receiver = new SketchReceiver(CurMode); // change the cability according to the user's option
            receiver.PushNotify += new EventHandler<CommandEvent>(receiver_PushNotify);

            gdiplus = new GdiPlusRendererPlugin(this.sketchCanvas);
            gdiplus.PushMessage += new EventHandler<CommandEvent>(gdiplus_PushMessage);            

            rts = new RealTimeStylus(this.sketchCanvas);
            
            rts.SyncPluginCollection.Add(gdiplus);
            rts.Enabled = false;

            // change the color back to black
            Black_Click(null, null);
        }


        private Colorpick currColor = Colorpick.Darkblue;
        private Brushpick currBrush = Brushpick.plPencil;

        /// <summary>
        /// Sending the message to the server for broadcasting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gdiplus_PushMessage(object sender, CommandEvent e)
        {
            receiver.CreateModifyStroke(e.Command);
        }


        /// <summary>
        /// Receving the messages from the server and render it on the screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void receiver_PushNotify(object sender, CommandEvent e)
        {
            switch(e.Command.Name)
            {
                case CommandNames.StartStroke:
                    gdiplus.AddNewStrokeFromOtherAgents(e.Command);
                    break;
                case CommandNames.SendPacketPoint:
                    gdiplus.AddNewPkPointFromOtherAgents((PacketPointCommand)e.Command);
                    break;
                case CommandNames.EndStroke:
                    
                    break;
                case CommandNames.Prepare:
                    this.BeginInvoke(new StartShowingOriginalDelegate(startTheGame));
                    break;
            }

        }

        private void startTheGame()
        {
            preOriginalTimer = new System.Windows.Forms.Timer();
            preOriginalTimer.Interval = 1000;
            preOriginalTimer.Tick += new EventHandler(prepareShowing_Tick);
            preOriginalTimer.Start();
        }

        void prepareShowing_Tick(object sender, EventArgs e)
        {
            this.lbCountdown.Visible = true;
            this.lbCountdown.Text = prepareDuration.ToString();

            System.Drawing.Point center = new System.Drawing.Point(this.sketchCanvas.Width / 2, this.sketchCanvas.Height / 2);
            this.lbCountdown.Location = new System.Drawing.Point(center.X - this.lbCountdown.Width/2, center.Y - this.lbCountdown.Height/2);

            if (prepareDuration <= 0)
            {
                this.lbCountdown.Visible = false;
                preOriginalTimer.Stop();
                this.sketchCanvas.BackgroundImage = SketchCanvas.originalPic;
                
                showingOriginalTimer = new System.Windows.Forms.Timer();
                showingOriginalTimer.Interval = 1000;
                showingOriginalTimer.Tick += new EventHandler(showingOriginalTimer_Tick);
                showingOriginalTimer.Start();
            }
            else
                prepareDuration--;
        }

        void showingOriginalTimer_Tick(object sender, EventArgs e)
        {
            int minute = this.showOriginalDuration / 60;
            int sec = this.showOriginalDuration % 60;

            string showmin = minute.ToString().Length == 1 ? "0" + minute.ToString() : minute.ToString();
            string showsec = sec.ToString().Length == 1 ? "0" + sec.ToString() : sec.ToString();

            this.lbSketchTimer.Text = showmin + ":" + showsec;

            if (this.showOriginalDuration <= 0)
            {
                this.sketchCanvas.BackgroundImage = null;
                showingOriginalTimer.Stop();

                sketchingTimer = new System.Windows.Forms.Timer();
                sketchingTimer.Interval = 1000;
                sketchingTimer.Tick += new EventHandler(sketchingTimer_Tick);
                sketchingTimer.Start();

                rts.Enabled = true;
            }
            else
                this.showOriginalDuration--;
        }

        void sketchingTimer_Tick(object sender, EventArgs e)
        {
            int minute = this.sketchDuration / 60;
            int sec = this.sketchDuration % 60;

            string showmin = minute.ToString().Length == 1 ? "0" + minute.ToString() : minute.ToString();
            string showsec = sec.ToString().Length == 1 ? "0" + sec.ToString() : sec.ToString();

            this.lbSketchTimer.Text = showmin + ":" + showsec;

            if (this.sketchDuration <= 0)
            {
                sketchingTimer.Stop();
                Feedback feedbackPage = new Feedback(this.sketchCanvas);
                feedbackPage.Show();
                // send the sketch to the next control
            }
            else
                this.sketchDuration--;
        }

        private void btShowOriginal_Click(object sender, EventArgs e)
        {
            if (isShowingOriginal)
            {
                isShowingOriginal = false;
                sketchCanvas.BackgroundImage = null;
            }
            else
            {
                isShowingOriginal = true;
                sketchCanvas.BackgroundImage = SketchCanvas.originalPic;
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            PropertyGrid propgrid = new PropertyGrid();            
            propgrid.Parent = this;            
            propgrid.SelectedObject = gdiplus.Pen;
            propgrid.Show();
        }

        private void btConnect_Click(object sender, EventArgs e)
        {
            if (this.txtBoxServerAddress.Text.Count() < 3 || (!this.txtBoxServerAddress.Text.Contains('.') && this.txtBoxServerAddress.Text != "localhost"))
            {
                MessageBox.Show("The input address is empty or invalid!");
                return;
            }

            receiver.Connect(this.txtBoxServerAddress.Text, 3999);
        }

        private void btSpy_Click(object sender, EventArgs e)
        {
            this.sketchCanvas.isSpyMode = true;

            spyTime = new System.Timers.Timer(SpyDuration);
            spyTime.Elapsed += new ElapsedEventHandler(spyTime_Elapsed);
            spyTime.Start();
        }

        void spyTime_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.sketchCanvas.isSpyMode = false;            
            this.sketchCanvas.Invalidate();
            spyTime.Elapsed -= new ElapsedEventHandler(spyTime_Elapsed);
        }

        private void btSpyOriginal_Click(object sender, EventArgs e)
        {
            this.sketchCanvas.isSpyOringalMode = true;
            spyTime = new System.Timers.Timer(SpyDuration);
            spyTime.Elapsed += new ElapsedEventHandler(spyOriginalTime_Elapsed);
            spyTime.Start();
        }

        void spyOriginalTime_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.sketchCanvas.isSpyOringalMode = false;
            this.sketchCanvas.BackgroundImage = null;
            this.sketchCanvas.Invalidate();
            spyTime.Elapsed -= new ElapsedEventHandler(spyTime_Elapsed);
        }

        private void ChangeBrushback(Brushpick preBrush)
        {
            switch (preBrush)
            {
                case Brushpick.plEraser:
                    this.plEraser.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.eraser_1;
                    break;
                case Brushpick.plPencil:
                    this.plPencil.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.pencil_1;
                    break;
                case Brushpick.plPen:
                    this.plPen.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.pen_1;
                    break;
                case Brushpick.plMarker:
                    this.plMarker.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.marker_1;
                    break;
                case Brushpick.plSharpie:
                    this.plSharpie.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.sharpie_1;
                    break;
                case Brushpick.plBrush:
                    this.plBrush.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.brush_1;
                    break;
            }
        }

        void plEraser_Click(object sender, System.EventArgs e)
        {
            ChangeBrushback(currBrush);
            this.plEraser.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.eraser_2;
            currBrush = Brushpick.plEraser;
        }

        void plPencil_Click(object sender, System.EventArgs e)
        {
            ChangeBrushback(currBrush);
            this.plPencil.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.pencil_2;
            currBrush = Brushpick.plPencil;
        }

        void plHome_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        void plPen_Click(object sender, System.EventArgs e)
        {
            ChangeBrushback(currBrush);
            this.plPen.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.pen_2;
            currBrush = Brushpick.plPen;
        }

        void plMarker_Click(object sender, System.EventArgs e)
        {
            ChangeBrushback(currBrush);
            this.plMarker.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.marker_2;
            currBrush = Brushpick.plMarker;
        }

        void plSharpie_Click(object sender, System.EventArgs e)
        {
            ChangeBrushback(currBrush);
            this.plSharpie.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.sharpie_2;
            currBrush = Brushpick.plSharpie;
        }

        void plBrush_Click(object sender, System.EventArgs e)
        {
            ChangeBrushback(currBrush);
            this.plBrush.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.brush_2;
            currBrush = Brushpick.plBrush;
        }

        private void Changeback(Colorpick preColor)
        {
            switch (preColor)
            {
                case Colorpick.Green:
                    this.Green.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._2_1;
                    break;
                case Colorpick.Darkblue:
                    this.Darkblue.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._1_2;
                    break;
                case Colorpick.Lightblue:
                    this.Lightblue.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._3_1;
                    break;
                case Colorpick.Purple:
                    this.Purple.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._4_1;
                    break;
                case Colorpick.Red:
                    this.Red.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._5_1;
                    break;
                case Colorpick.Yellow:
                    this.Yellow.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._6_1;
                    break;
                case Colorpick.Grey:
                    this.Grey.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._7_1;
                    break;
                case Colorpick.White:
                    this.White.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._8_1;
                    break;
                case Colorpick.Black:
                    this.Black.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._9_1;
                    break;
            }
        }

        void Green_Click(object sender, System.EventArgs e)
        {
            this.gdiplus.Pen.Color = Color.FromArgb(0, 89, 82);
            Changeback(currColor);
            this.Green.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._2_2;
            currColor = Colorpick.Green;
        }

        void Lightblue_Click(object sender, System.EventArgs e)
        {
            this.gdiplus.Pen.Color = Color.FromArgb(63, 138, 168);
            Changeback(currColor);
            this.Lightblue.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._3_2;
            currColor = Colorpick.Lightblue;
        }

        void Darkblue_Click(object sender, System.EventArgs e)
        {
            this.gdiplus.Pen.Color = Color.FromArgb(0, 91, 127);
            Changeback(currColor);
            this.Darkblue.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._1_1;
            currColor = Colorpick.Darkblue;
        }

        void Purple_Click(object sender, System.EventArgs e)
        {
            this.gdiplus.Pen.Color = Color.FromArgb(96, 92, 168);
            Changeback(currColor);
            this.Purple.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._4_2;
            currColor = Colorpick.Purple;
        }

        void Red_Click(object sender, System.EventArgs e)
        {
            this.gdiplus.Pen.Color = Color.FromArgb(109, 0, 34);
            Changeback(currColor);
            this.Red.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._5_2;
            currColor = Colorpick.Red;
        }

        void Yellow_Click(object sender, System.EventArgs e)
        {
            this.gdiplus.Pen.Color = Color.FromArgb(165, 134, 100);
            Changeback(currColor);
            this.Yellow.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._6_2;
            currColor = Colorpick.Yellow;
        }

        void Grey_Click(object sender, System.EventArgs e)
        {
            this.gdiplus.Pen.Color = Color.FromArgb(149, 149, 149);
            Changeback(currColor);
            this.Grey.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._7_2;
            currColor = Colorpick.Grey;
        }

        void White_Click(object sender, System.EventArgs e)
        {
            this.gdiplus.Pen.Color = Color.FromArgb(255, 255, 255);
            Changeback(currColor);
            this.White.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._8_2;
            currColor = Colorpick.White;
        }

        void Black_Click(object sender, System.EventArgs e)
        {
            this.gdiplus.Pen.Color = Color.FromArgb(1, 1, 1);
            Changeback(currColor);
            this.Black.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._9_2;
            currColor = Colorpick.Black;
        }    
    }
}
