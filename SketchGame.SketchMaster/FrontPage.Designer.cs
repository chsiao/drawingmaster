﻿namespace SketchGame.SketchMaster
{
    partial class FrontPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrontPage));
            this.plMyWork = new System.Windows.Forms.Panel();
            this.plBest = new System.Windows.Forms.Panel();
            this.plRandom = new System.Windows.Forms.Panel();
            this.plExit = new System.Windows.Forms.Panel();
            this.plPref = new System.Windows.Forms.Panel();
            this.btSingle = new System.Windows.Forms.Button();
            this.btMultiple = new System.Windows.Forms.Button();
            this.btCoSketch = new System.Windows.Forms.Button();
            this.btPassOn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // plMyWork
            // 
            this.plMyWork.BackColor = System.Drawing.Color.Transparent;
            this.plMyWork.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.MyWork_1;
            this.plMyWork.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.plMyWork.Location = new System.Drawing.Point(96, 579);
            this.plMyWork.Name = "plMyWork";
            this.plMyWork.Size = new System.Drawing.Size(376, 198);
            this.plMyWork.TabIndex = 0;
            this.plMyWork.Click += new System.EventHandler(this.plMyWork_Click);
            this.plMyWork.MouseDown += new System.Windows.Forms.MouseEventHandler(this.plMyWork_MouseDown);
            this.plMyWork.MouseUp += new System.Windows.Forms.MouseEventHandler(this.plMyWork_MouseUp);
            // 
            // plBest
            // 
            this.plBest.BackColor = System.Drawing.Color.Transparent;
            this.plBest.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.Best_1;
            this.plBest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.plBest.Location = new System.Drawing.Point(504, 580);
            this.plBest.Name = "plBest";
            this.plBest.Size = new System.Drawing.Size(375, 189);
            this.plBest.TabIndex = 1;
            this.plBest.Click += new System.EventHandler(this.plBest_Click);
            this.plBest.MouseDown += new System.Windows.Forms.MouseEventHandler(this.plBest_MouseDown);
            this.plBest.MouseUp += new System.Windows.Forms.MouseEventHandler(this.plBest_MouseUp);
            // 
            // plRandom
            // 
            this.plRandom.BackColor = System.Drawing.Color.Transparent;
            this.plRandom.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.random_1;
            this.plRandom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.plRandom.Location = new System.Drawing.Point(893, 582);
            this.plRandom.Name = "plRandom";
            this.plRandom.Size = new System.Drawing.Size(389, 185);
            this.plRandom.TabIndex = 2;
            this.plRandom.Click += new System.EventHandler(this.plRandom_Click);
            this.plRandom.MouseDown += new System.Windows.Forms.MouseEventHandler(this.plRandom_MouseDown);
            this.plRandom.MouseUp += new System.Windows.Forms.MouseEventHandler(this.plRandom_MouseUp);
            // 
            // plExit
            // 
            this.plExit.BackColor = System.Drawing.Color.Transparent;
            this.plExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("plExit.BackgroundImage")));
            this.plExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.plExit.Location = new System.Drawing.Point(1222, 17);
            this.plExit.Name = "plExit";
            this.plExit.Size = new System.Drawing.Size(60, 52);
            this.plExit.TabIndex = 3;
            this.plExit.Click += new System.EventHandler(this.plExit_Click);
            // 
            // plPref
            // 
            this.plPref.BackColor = System.Drawing.Color.Transparent;
            this.plPref.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.PreferenceButton;
            this.plPref.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.plPref.Location = new System.Drawing.Point(1154, 12);
            this.plPref.Name = "plPref";
            this.plPref.Size = new System.Drawing.Size(62, 57);
            this.plPref.TabIndex = 4;
            this.plPref.Click += new System.EventHandler(this.plPref_Click);
            // 
            // btSingle
            // 
            this.btSingle.BackColor = System.Drawing.Color.Transparent;
            this.btSingle.Font = new System.Drawing.Font("BankGothic Lt BT", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSingle.Location = new System.Drawing.Point(893, 193);
            this.btSingle.Name = "btSingle";
            this.btSingle.Size = new System.Drawing.Size(298, 57);
            this.btSingle.TabIndex = 5;
            this.btSingle.Text = "Single";
            this.btSingle.UseVisualStyleBackColor = false;
            this.btSingle.Click += new System.EventHandler(this.btSingle_Click);
            // 
            // btMultiple
            // 
            this.btMultiple.Font = new System.Drawing.Font("BankGothic Lt BT", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btMultiple.Location = new System.Drawing.Point(893, 260);
            this.btMultiple.Name = "btMultiple";
            this.btMultiple.Size = new System.Drawing.Size(298, 57);
            this.btMultiple.TabIndex = 6;
            this.btMultiple.Text = "Multiple";
            this.btMultiple.UseVisualStyleBackColor = true;
            this.btMultiple.Click += new System.EventHandler(this.btMultiple_Click);
            // 
            // btCoSketch
            // 
            this.btCoSketch.Font = new System.Drawing.Font("BankGothic Lt BT", 20F);
            this.btCoSketch.Location = new System.Drawing.Point(893, 327);
            this.btCoSketch.Name = "btCoSketch";
            this.btCoSketch.Size = new System.Drawing.Size(298, 57);
            this.btCoSketch.TabIndex = 7;
            this.btCoSketch.Text = "Co-Sketch";
            this.btCoSketch.UseVisualStyleBackColor = true;
            this.btCoSketch.Click += new System.EventHandler(this.btCoSketch_Click);
            // 
            // btPassOn
            // 
            this.btPassOn.Font = new System.Drawing.Font("BankGothic Lt BT", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPassOn.Location = new System.Drawing.Point(893, 394);
            this.btPassOn.Name = "btPassOn";
            this.btPassOn.Size = new System.Drawing.Size(298, 57);
            this.btPassOn.TabIndex = 8;
            this.btPassOn.Text = "Pass-on";
            this.btPassOn.UseVisualStyleBackColor = true;
            this.btPassOn.Click += new System.EventHandler(this.btPassOn_Click);
            // 
            // FrontPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.Home_Background;
            this.ClientSize = new System.Drawing.Size(1336, 768);
            this.Controls.Add(this.btPassOn);
            this.Controls.Add(this.btCoSketch);
            this.Controls.Add(this.btMultiple);
            this.Controls.Add(this.btSingle);
            this.Controls.Add(this.plPref);
            this.Controls.Add(this.plExit);
            this.Controls.Add(this.plRandom);
            this.Controls.Add(this.plBest);
            this.Controls.Add(this.plMyWork);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrontPage";
            this.Text = "FrontPage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel plMyWork;
        private System.Windows.Forms.Panel plBest;
        private System.Windows.Forms.Panel plRandom;
        private System.Windows.Forms.Panel plExit;
        private System.Windows.Forms.Panel plPref;
        private System.Windows.Forms.Button btSingle;
        private System.Windows.Forms.Button btMultiple;
        private System.Windows.Forms.Button btCoSketch;
        private System.Windows.Forms.Button btPassOn;

    }
}