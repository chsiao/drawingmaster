﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SketchGame.Core;
using SketchGame.Server;

namespace SketchGame.SketchMaster
{
    public partial class SelPicPage : Form
    {

        private AgentCapabilities GameMode;

        public SelPicPage(AgentCapabilities mode)
        {
            InitializeComponent();
            this.GameMode = mode;
            switch(mode)
            {
                case AgentCapabilities.CoSketchSend:
                    this.lbCurMode.Text = "Co-Sketch Mode";
                    break;
                case AgentCapabilities.SingleMode:
                    this.lbCurMode.Text = "Single Mode";
                    break;
                case AgentCapabilities.SketchCompetetion:
                    this.lbCurMode.Text = "Multiple Player Mode";
                    break;
            }

        }

        private void btEasy_CheckedChanged(object sender, EventArgs e)
        {
            if (btEasy.Checked)
            {
                this.plPicture.BackgroundImage = Properties.Resources.piceasy;
            }
            else
            {
                this.plPicture.BackgroundImage = Properties.Resources.placeholder;
            }
        }

        private void btMedium_CheckedChanged(object sender, EventArgs e)
        {
            if (btMedium.Checked)
            {
                this.plPicture.BackgroundImage = Properties.Resources.picmid;
                this.plCompexFigure.BackgroundImage = Properties.Resources.ComplexFigure;
            }
            else
            {
                this.plPicture.BackgroundImage = Properties.Resources.placeholder;
                this.plCompexFigure.BackgroundImage = Properties.Resources.placeholder;
            }
        }

        private void btHard_CheckedChanged(object sender, EventArgs e)
        {
            if (btHard.Checked)
            {
                this.plPicture.BackgroundImage = Properties.Resources.pichard;
            }
            else
            {
                this.plPicture.BackgroundImage = Properties.Resources.placeholder;
            }
        }


        void plPicture_Click(object sender, System.EventArgs e)
        {
            Level level = 0;
            if (btEasy.Checked)
                level = Level.easy;
            else if (btMedium.Checked)
                level = Level.medium;
            else if (btHard.Checked)
                level = Level.hard;

            SketchMaster StartingGame = new SketchMaster(this.GameMode, level);
            StartingGame.Show();
        }

        void plCompexFigure_Click(object sender, System.EventArgs e)
        {
            SketchMaster StartingGame = new SketchMaster(this.GameMode, Level.complexFigure);
            StartingGame.Show();
        }


        void plHome_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
