﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SketchGame.SketchMaster
{
    public partial class FrontPage : Form
    {
        string randomPath = "";
        string myworkPath = "";
        string bestPath = "";


        public FrontPage()
        {
            InitializeComponent();
            string path = Application.StartupPath + @"\gameUI\index.html";            

        }

        private void btSingle_Click(object sender, EventArgs e)
        {
            SelPicPage picSelection = new SelPicPage(SketchGame.Server.AgentCapabilities.SingleMode);
            picSelection.Show();
        }

        private void btMultiple_Click(object sender, EventArgs e)
        {
            SelPicPage picSelection = new SelPicPage(SketchGame.Server.AgentCapabilities.SketchCompetetion);
            picSelection.Show();
        }

        private void btCoSketch_Click(object sender, EventArgs e)
        {
            SelPicPage picSelection = new SelPicPage(SketchGame.Server.AgentCapabilities.CoSketchSend);
            picSelection.Show();
        }

        private void btPassOn_Click(object sender, EventArgs e)
        {

        }


        void plExit_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        void plPref_Click(object sender, System.EventArgs e)
        {
            
        }


        void plMyWork_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.plMyWork.BackgroundImage = Properties.Resources.MyWork_1;
        }

        void plMyWork_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.plMyWork.BackgroundImage = Properties.Resources.MyWork;
        }

        void plBest_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.plBest.BackgroundImage = Properties.Resources.Best_1;
        }

        void plBest_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.plBest.BackgroundImage = Properties.Resources.Best;
        }

        void plRandom_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.plRandom.BackgroundImageLayout = ImageLayout.None;
            this.plRandom.BackgroundImage = Properties.Resources.random_1;
        }

        void plRandom_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.plRandom.BackgroundImageLayout = ImageLayout.Center;
            this.plRandom.BackgroundImage = Properties.Resources.Random;
        }

        void plRandom_Click(object sender, System.EventArgs e)
        {
            
        }

        void plMyWork_Click(object sender, System.EventArgs e)
        {
            
        }

        void plBest_Click(object sender, System.EventArgs e)
        {
            
        }
    }
}
