﻿namespace SketchGame.SketchMaster
{
    partial class SelPicPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plHome = new System.Windows.Forms.Panel();
            this.plPlaceHolder1 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.plCompexFigure = new System.Windows.Forms.Panel();
            this.plPicture = new System.Windows.Forms.Panel();
            this.btEasy = new System.Windows.Forms.RadioButton();
            this.btMedium = new System.Windows.Forms.RadioButton();
            this.btHard = new System.Windows.Forms.RadioButton();
            this.lbCurMode = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // plHome
            // 
            this.plHome.BackColor = System.Drawing.Color.Transparent;
            this.plHome.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.HomeButton;
            this.plHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.plHome.Location = new System.Drawing.Point(48, 681);
            this.plHome.Name = "plHome";
            this.plHome.Size = new System.Drawing.Size(64, 63);
            this.plHome.TabIndex = 0;
            this.plHome.Click += new System.EventHandler(this.plHome_Click);
            // 
            // plPlaceHolder1
            // 
            this.plPlaceHolder1.BackColor = System.Drawing.Color.Transparent;
            this.plPlaceHolder1.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.placeholder;
            this.plPlaceHolder1.Location = new System.Drawing.Point(80, 374);
            this.plPlaceHolder1.Name = "plPlaceHolder1";
            this.plPlaceHolder1.Size = new System.Drawing.Size(378, 247);
            this.plPlaceHolder1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.placeholder;
            this.panel1.Location = new System.Drawing.Point(494, 374);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(378, 247);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.placeholder;
            this.panel2.Location = new System.Drawing.Point(908, 374);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(378, 247);
            this.panel2.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.placeholder;
            this.panel3.Location = new System.Drawing.Point(908, 92);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(378, 247);
            this.panel3.TabIndex = 5;
            // 
            // plCompexFigure
            // 
            this.plCompexFigure.BackColor = System.Drawing.Color.Transparent;
            this.plCompexFigure.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.placeholder;
            this.plCompexFigure.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.plCompexFigure.Location = new System.Drawing.Point(494, 92);
            this.plCompexFigure.Name = "plCompexFigure";
            this.plCompexFigure.Size = new System.Drawing.Size(378, 247);
            this.plCompexFigure.TabIndex = 4;
            this.plCompexFigure.Click += new System.EventHandler(this.plCompexFigure_Click);
            // 
            // plPicture
            // 
            this.plPicture.BackColor = System.Drawing.Color.Transparent;
            this.plPicture.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.piceasy;
            this.plPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.plPicture.Location = new System.Drawing.Point(80, 92);
            this.plPicture.Name = "plPicture";
            this.plPicture.Size = new System.Drawing.Size(378, 247);
            this.plPicture.TabIndex = 3;
            this.plPicture.Click += new System.EventHandler(this.plPicture_Click);
            // 
            // btEasy
            // 
            this.btEasy.Appearance = System.Windows.Forms.Appearance.Button;
            this.btEasy.Checked = true;
            this.btEasy.Font = new System.Drawing.Font("BankGothic Lt BT", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEasy.Location = new System.Drawing.Point(402, 652);
            this.btEasy.Name = "btEasy";
            this.btEasy.Size = new System.Drawing.Size(167, 54);
            this.btEasy.TabIndex = 7;
            this.btEasy.TabStop = true;
            this.btEasy.Text = "Easy";
            this.btEasy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btEasy.UseVisualStyleBackColor = true;
            this.btEasy.CheckedChanged += new System.EventHandler(this.btEasy_CheckedChanged);
            // 
            // btMedium
            // 
            this.btMedium.Appearance = System.Windows.Forms.Appearance.Button;
            this.btMedium.Font = new System.Drawing.Font("BankGothic Lt BT", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btMedium.Location = new System.Drawing.Point(603, 652);
            this.btMedium.Name = "btMedium";
            this.btMedium.Size = new System.Drawing.Size(167, 54);
            this.btMedium.TabIndex = 8;
            this.btMedium.Text = "Medium";
            this.btMedium.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btMedium.UseVisualStyleBackColor = true;
            this.btMedium.CheckedChanged += new System.EventHandler(this.btMedium_CheckedChanged);
            // 
            // btHard
            // 
            this.btHard.Appearance = System.Windows.Forms.Appearance.Button;
            this.btHard.Font = new System.Drawing.Font("BankGothic Lt BT", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btHard.Location = new System.Drawing.Point(804, 652);
            this.btHard.Name = "btHard";
            this.btHard.Size = new System.Drawing.Size(167, 54);
            this.btHard.TabIndex = 9;
            this.btHard.Text = "Hard";
            this.btHard.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btHard.UseVisualStyleBackColor = true;
            this.btHard.CheckedChanged += new System.EventHandler(this.btHard_CheckedChanged);
            // 
            // lbCurMode
            // 
            this.lbCurMode.AutoSize = true;
            this.lbCurMode.BackColor = System.Drawing.Color.Transparent;
            this.lbCurMode.Font = new System.Drawing.Font("BankGothic Lt BT", 18F);
            this.lbCurMode.Location = new System.Drawing.Point(80, 13);
            this.lbCurMode.Name = "lbCurMode";
            this.lbCurMode.Size = new System.Drawing.Size(0, 25);
            this.lbCurMode.TabIndex = 10;
            // 
            // SelPicPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.SelPic_Back;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.lbCurMode);
            this.Controls.Add(this.btHard);
            this.Controls.Add(this.btMedium);
            this.Controls.Add(this.btEasy);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.plCompexFigure);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.plPicture);
            this.Controls.Add(this.plPlaceHolder1);
            this.Controls.Add(this.plHome);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SelPicPage";
            this.Text = "SelPicPage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }        

        #endregion

        private System.Windows.Forms.Panel plHome;
        private System.Windows.Forms.Panel plPlaceHolder1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel plCompexFigure;
        private System.Windows.Forms.Panel plPicture;
        private System.Windows.Forms.RadioButton btEasy;
        private System.Windows.Forms.RadioButton btMedium;
        private System.Windows.Forms.RadioButton btHard;
        private System.Windows.Forms.Label lbCurMode;
    }
}