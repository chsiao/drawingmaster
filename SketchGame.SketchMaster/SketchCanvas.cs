﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

using SketchGame.Core;

namespace SketchGame.SketchMaster
{
    public partial class SketchCanvas : Panel
    {
        public static SketchGame.Server.AgentCapabilities CurMode { get; set; }
        public static Bitmap originalPic { get; set; }

        public bool isSpyMode { get; set; }
        public bool isSpyOringalMode { get; set; }
        bool isDrawingStrokes { get; set; }

        private Rectangle SpyCircle { get; set; }        
        private Region clipRegion = new System.Drawing.Region();
        private Bitmap scope;
        
        private Point oriPicDrawingLocation;
        public object drawingLock = new object();

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<PenStroke> myStrokes
        {
            get;
            set;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Dictionary<string, List<PenStroke>> strokesFromOther
        {
            get;
            set;
        }

        Pen currentPen;

        public SketchCanvas()
        {
            InitializeComponent();
            
            this.isDrawingStrokes = true;
            this.myStrokes = new List<PenStroke>();
            this.strokesFromOther = new Dictionary<string, List<PenStroke>>();
            this.MouseMove += new MouseEventHandler(SketchCanvas_MouseMove);

            this.scope = Properties.Resources.spy;                        

            this.DoubleBuffered = true;
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        void SketchCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.isSpyMode || this.isSpyOringalMode)
            {                                
                this.SpyCircle = new Rectangle(e.X - 75, e.Y - 75, 150, 150);

                Rectangle clipCircle = new Rectangle(e.X - 42, e.Y - 42, 135, 135);

                GraphicsPath circlePath = new GraphicsPath();
                circlePath.AddEllipse(clipCircle);

                clipRegion = new System.Drawing.Region(circlePath);


                this.Invalidate();
            }
            else
            {
                // utilize clipregion as a flag
                if (clipRegion != null)
                    this.Invalidate();

                clipRegion = null;                
            }
        }

        private void RenderStoredStrokes(List<PenStroke> strokes, Graphics g)
        {
            lock (this.drawingLock)
            {
                foreach (PenStroke penSt in strokes)
                {
                    currentPen = new System.Drawing.Pen(penSt.Color);
                    currentPen.Width = 53;
                    PacketPoint prePt = null;

                    foreach (PacketPoint pt in penSt.PacketPoints)
                    {
                        if (prePt != null)
                        {
                            if (pt.Pressure != 0)
                            {
                                double width = 53 * (1 + (pt.Pressure - 128f) / 16);
                                currentPen.Width = (float)width;
                            }

                            g.DrawLine(currentPen, new Point(prePt.X, prePt.Y), new Point(pt.X, pt.Y));
                        }

                        prePt = pt;
                    }
                }               
            }
        }

        public Bitmap GetMyDrawingInBitmap()
        {
            Bitmap mydrawing = new System.Drawing.Bitmap(this.Width, this.Height);
            Graphics g = Graphics.FromImage(mydrawing);
            g.FillRectangle(Brushes.White, 0, 0, this.Width, this.Height);

            g.PageUnit = GraphicsUnit.Millimeter;
            g.PageScale = 0.01f;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            RenderStoredStrokes(myStrokes, g);

            return mydrawing;
        }

        public Bitmap GetOtherDrawingInBitmap()
        {
            Bitmap otherDrawing = new System.Drawing.Bitmap(this.Width, this.Height);
            Graphics g = Graphics.FromImage(otherDrawing);
            g.FillRectangle(Brushes.White, 0, 0, this.Width, this.Height);

            g.PageUnit = GraphicsUnit.Millimeter;
            g.PageScale = 0.01f;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            foreach (List<PenStroke> otherSts in strokesFromOther.Values)
            {
                RenderStoredStrokes(otherSts, g);
            }

            return otherDrawing;
        }


        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            lock (drawingLock)
            {
                Graphics g = e.Graphics;


                if (isDrawingStrokes)
                {                    
                    g.PageUnit = GraphicsUnit.Millimeter;
                    g.PageScale = 0.01f;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                    
                    RenderStoredStrokes(myStrokes, g);

                    if (isSpyOringalMode || isSpyMode)
                    {
                        if (clipRegion != null)
                        {
                            g.PageUnit = GraphicsUnit.Display;
                            g.PageScale = 1;

                            //g.FillEllipse(Brushes.White, this.SpyCircle);
                            g.FillRegion(Brushes.White, clipRegion);
                            g.DrawImage(this.scope, this.SpyCircle.Location);

                            g.Clip = clipRegion;                                                       
                        }

                        if (isSpyOringalMode)
                        {
                            oriPicDrawingLocation = new System.Drawing.Point((this.Width - SketchCanvas.originalPic.Width) / 2, (this.Height - SketchCanvas.originalPic.Height) / 2);
                            g.DrawImage(originalPic, oriPicDrawingLocation);
                        }

                        if (SketchCanvas.CurMode == Server.AgentCapabilities.SketchCompetetion)
                        {
                            g.PageUnit = GraphicsUnit.Millimeter;
                            g.PageScale = 0.01f;

                            foreach (List<PenStroke> otherSts in strokesFromOther.Values)
                            {
                                RenderStoredStrokes(otherSts, g);
                            }
                        }
                    }
                }

                base.OnPaint(e);
            }
        }
    }
}
