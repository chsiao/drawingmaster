﻿using System.Windows.Forms;

namespace SketchGame.SketchMaster
{

    partial class SketchMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.plHome = new System.Windows.Forms.Panel();
            this.btConnect = new System.Windows.Forms.Button();
            this.txtBoxServerAddress = new System.Windows.Forms.TextBox();
            this.btSpyOriginal = new System.Windows.Forms.Panel();
            this.btSpy = new System.Windows.Forms.Panel();
            this.plMarker = new System.Windows.Forms.Panel();
            this.plSharpie = new System.Windows.Forms.Panel();
            this.plEraser = new System.Windows.Forms.Panel();
            this.plPencil = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.Darkblue = new System.Windows.Forms.Panel();
            this.Black = new System.Windows.Forms.Panel();
            this.White = new System.Windows.Forms.Panel();
            this.Grey = new System.Windows.Forms.Panel();
            this.Yellow = new System.Windows.Forms.Panel();
            this.Red = new System.Windows.Forms.Panel();
            this.Purple = new System.Windows.Forms.Panel();
            this.Green = new System.Windows.Forms.Panel();
            this.Lightblue = new System.Windows.Forms.Panel();
            this.plBrush = new System.Windows.Forms.Panel();
            this.plPen = new System.Windows.Forms.Panel();
            this.sketchCanvas = new SketchGame.SketchMaster.SketchCanvas();
            this.lbCountdown = new System.Windows.Forms.Label();
            this.lbSketchTimer = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.sketchCanvas.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.tool_bar;
            this.panel1.Controls.Add(this.lbSketchTimer);
            this.panel1.Controls.Add(this.plHome);
            this.panel1.Controls.Add(this.btConnect);
            this.panel1.Controls.Add(this.txtBoxServerAddress);
            this.panel1.Controls.Add(this.btSpyOriginal);
            this.panel1.Controls.Add(this.btSpy);
            this.panel1.Controls.Add(this.plMarker);
            this.panel1.Controls.Add(this.plSharpie);
            this.panel1.Controls.Add(this.plEraser);
            this.panel1.Controls.Add(this.plPencil);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.plBrush);
            this.panel1.Controls.Add(this.plPen);
            this.panel1.Location = new System.Drawing.Point(1, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(191, 770);
            this.panel1.TabIndex = 1;
            // 
            // plHome
            // 
            this.plHome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.plHome.BackColor = System.Drawing.Color.Transparent;
            this.plHome.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.HomeButton;
            this.plHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.plHome.Location = new System.Drawing.Point(20, 678);
            this.plHome.Name = "plHome";
            this.plHome.Size = new System.Drawing.Size(55, 52);
            this.plHome.TabIndex = 2;
            this.plHome.Click += new System.EventHandler(this.plHome_Click);
            // 
            // btConnect
            // 
            this.btConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btConnect.Location = new System.Drawing.Point(156, 734);
            this.btConnect.Name = "btConnect";
            this.btConnect.Size = new System.Drawing.Size(28, 23);
            this.btConnect.TabIndex = 4;
            this.btConnect.Text = "go";
            this.btConnect.UseVisualStyleBackColor = true;
            this.btConnect.Click += new System.EventHandler(this.btConnect_Click);
            // 
            // txtBoxServerAddress
            // 
            this.txtBoxServerAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBoxServerAddress.Location = new System.Drawing.Point(10, 736);
            this.txtBoxServerAddress.Name = "txtBoxServerAddress";
            this.txtBoxServerAddress.Size = new System.Drawing.Size(140, 20);
            this.txtBoxServerAddress.TabIndex = 3;
            // 
            // btSpyOriginal
            // 
            this.btSpyOriginal.BackColor = System.Drawing.Color.Transparent;
            this.btSpyOriginal.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.detect2_1;
            this.btSpyOriginal.Location = new System.Drawing.Point(95, 606);
            this.btSpyOriginal.Name = "btSpyOriginal";
            this.btSpyOriginal.Size = new System.Drawing.Size(55, 55);
            this.btSpyOriginal.TabIndex = 1;
            this.btSpyOriginal.Click += new System.EventHandler(this.btSpyOriginal_Click);
            // 
            // btSpy
            // 
            this.btSpy.BackColor = System.Drawing.Color.Transparent;
            this.btSpy.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.detect1_1;
            this.btSpy.Location = new System.Drawing.Point(20, 606);
            this.btSpy.Name = "btSpy";
            this.btSpy.Size = new System.Drawing.Size(55, 55);
            this.btSpy.TabIndex = 1;
            this.btSpy.Click += new System.EventHandler(this.btSpy_Click);
            // 
            // plMarker
            // 
            this.plMarker.BackColor = System.Drawing.Color.Transparent;
            this.plMarker.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.marker_1;
            this.plMarker.Location = new System.Drawing.Point(-20, 205);
            this.plMarker.Name = "plMarker";
            this.plMarker.Size = new System.Drawing.Size(176, 81);
            this.plMarker.TabIndex = 1;
            this.plMarker.Click += new System.EventHandler(this.plMarker_Click);
            // 
            // plSharpie
            // 
            this.plSharpie.BackColor = System.Drawing.Color.Transparent;
            this.plSharpie.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.sharpie_1;
            this.plSharpie.Location = new System.Drawing.Point(-20, 275);
            this.plSharpie.Name = "plSharpie";
            this.plSharpie.Size = new System.Drawing.Size(176, 76);
            this.plSharpie.TabIndex = 1;
            this.plSharpie.Click += new System.EventHandler(this.plSharpie_Click);
            // 
            // plEraser
            // 
            this.plEraser.BackColor = System.Drawing.Color.Transparent;
            this.plEraser.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.eraser_1;
            this.plEraser.Location = new System.Drawing.Point(-20, -5);
            this.plEraser.Name = "plEraser";
            this.plEraser.Size = new System.Drawing.Size(147, 90);
            this.plEraser.TabIndex = 1;
            this.plEraser.Click += new System.EventHandler(this.plEraser_Click);
            // 
            // plPencil
            // 
            this.plPencil.BackColor = System.Drawing.Color.Transparent;
            this.plPencil.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.pencil_2;
            this.plPencil.Location = new System.Drawing.Point(-20, 75);
            this.plPencil.Name = "plPencil";
            this.plPencil.Size = new System.Drawing.Size(176, 70);
            this.plPencil.TabIndex = 2;
            this.plPencil.Click += new System.EventHandler(this.plPencil_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.color_plate;
            this.panel8.Controls.Add(this.Darkblue);
            this.panel8.Controls.Add(this.Black);
            this.panel8.Controls.Add(this.White);
            this.panel8.Controls.Add(this.Grey);
            this.panel8.Controls.Add(this.Yellow);
            this.panel8.Controls.Add(this.Red);
            this.panel8.Controls.Add(this.Purple);
            this.panel8.Controls.Add(this.Green);
            this.panel8.Controls.Add(this.Lightblue);
            this.panel8.Location = new System.Drawing.Point(-5, 400);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(181, 200);
            this.panel8.TabIndex = 1;
            // 
            // Darkblue
            // 
            this.Darkblue.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._1_1;
            this.Darkblue.Location = new System.Drawing.Point(15, 14);
            this.Darkblue.Name = "Darkblue";
            this.Darkblue.Size = new System.Drawing.Size(49, 49);
            this.Darkblue.TabIndex = 1;
            this.Darkblue.Click += new System.EventHandler(this.Darkblue_Click);
            // 
            // Black
            // 
            this.Black.BackColor = System.Drawing.Color.Transparent;
            this.Black.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._9_1;
            this.Black.Location = new System.Drawing.Point(125, 124);
            this.Black.Name = "Black";
            this.Black.Size = new System.Drawing.Size(49, 49);
            this.Black.TabIndex = 1;
            this.Black.Click += new System.EventHandler(this.Black_Click);
            // 
            // White
            // 
            this.White.BackColor = System.Drawing.Color.Transparent;
            this.White.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._8_1;
            this.White.Location = new System.Drawing.Point(70, 124);
            this.White.Name = "White";
            this.White.Size = new System.Drawing.Size(49, 49);
            this.White.TabIndex = 1;
            this.White.Click += new System.EventHandler(this.White_Click);
            // 
            // Grey
            // 
            this.Grey.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._7_1;
            this.Grey.Location = new System.Drawing.Point(15, 124);
            this.Grey.Name = "Grey";
            this.Grey.Size = new System.Drawing.Size(49, 49);
            this.Grey.TabIndex = 1;
            this.Grey.Click += new System.EventHandler(this.Grey_Click);
            // 
            // Yellow
            // 
            this.Yellow.BackColor = System.Drawing.Color.Transparent;
            this.Yellow.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._6_1;
            this.Yellow.Location = new System.Drawing.Point(125, 69);
            this.Yellow.Name = "Yellow";
            this.Yellow.Size = new System.Drawing.Size(49, 49);
            this.Yellow.TabIndex = 1;
            this.Yellow.Click += new System.EventHandler(this.Yellow_Click);
            // 
            // Red
            // 
            this.Red.BackColor = System.Drawing.Color.Transparent;
            this.Red.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._5_1;
            this.Red.Location = new System.Drawing.Point(70, 69);
            this.Red.Name = "Red";
            this.Red.Size = new System.Drawing.Size(49, 49);
            this.Red.TabIndex = 1;
            this.Red.Click += new System.EventHandler(this.Red_Click);
            // 
            // Purple
            // 
            this.Purple.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._4_1;
            this.Purple.Location = new System.Drawing.Point(15, 69);
            this.Purple.Name = "Purple";
            this.Purple.Size = new System.Drawing.Size(49, 49);
            this.Purple.TabIndex = 1;
            this.Purple.Click += new System.EventHandler(this.Purple_Click);
            // 
            // Green
            // 
            this.Green.BackColor = System.Drawing.Color.Transparent;
            this.Green.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._2_1;
            this.Green.Location = new System.Drawing.Point(69, 14);
            this.Green.Name = "Green";
            this.Green.Size = new System.Drawing.Size(49, 49);
            this.Green.TabIndex = 1;
            this.Green.Click += new System.EventHandler(this.Green_Click);
            // 
            // Lightblue
            // 
            this.Lightblue.BackColor = System.Drawing.Color.Transparent;
            this.Lightblue.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources._3_1;
            this.Lightblue.Location = new System.Drawing.Point(124, 14);
            this.Lightblue.Name = "Lightblue";
            this.Lightblue.Size = new System.Drawing.Size(49, 49);
            this.Lightblue.TabIndex = 1;
            this.Lightblue.Click += new System.EventHandler(this.Lightblue_Click);
            // 
            // plBrush
            // 
            this.plBrush.BackColor = System.Drawing.Color.Transparent;
            this.plBrush.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.brush_1;
            this.plBrush.Location = new System.Drawing.Point(-20, 340);
            this.plBrush.Name = "plBrush";
            this.plBrush.Size = new System.Drawing.Size(176, 81);
            this.plBrush.TabIndex = 1;
            this.plBrush.Click += new System.EventHandler(this.plBrush_Click);
            // 
            // plPen
            // 
            this.plPen.BackColor = System.Drawing.Color.Transparent;
            this.plPen.BackgroundImage = global::SketchGame.SketchMaster.Properties.Resources.pen_1;
            this.plPen.Location = new System.Drawing.Point(-20, 135);
            this.plPen.Name = "plPen";
            this.plPen.Size = new System.Drawing.Size(176, 81);
            this.plPen.TabIndex = 2;
            this.plPen.Click += new System.EventHandler(this.plPen_Click);
            // 
            // sketchCanvas
            // 
            this.sketchCanvas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.sketchCanvas.BackColor = System.Drawing.Color.White;
            this.sketchCanvas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.sketchCanvas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.sketchCanvas.Controls.Add(this.lbCountdown);
            this.sketchCanvas.isSpyMode = false;
            this.sketchCanvas.isSpyOringalMode = false;
            this.sketchCanvas.Location = new System.Drawing.Point(190, 0);
            this.sketchCanvas.Name = "sketchCanvas";
            this.sketchCanvas.Size = new System.Drawing.Size(1175, 768);
            this.sketchCanvas.TabIndex = 1;
            // 
            // lbCountdown
            // 
            this.lbCountdown.AutoSize = true;
            this.lbCountdown.BackColor = System.Drawing.Color.Transparent;
            this.lbCountdown.Font = new System.Drawing.Font("BankGothic Lt BT", 120F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCountdown.Location = new System.Drawing.Point(573, 290);
            this.lbCountdown.Name = "lbCountdown";
            this.lbCountdown.Size = new System.Drawing.Size(0, 167);
            this.lbCountdown.TabIndex = 2;
            // 
            // lbSketchTimer
            // 
            this.lbSketchTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbSketchTimer.AutoSize = true;
            this.lbSketchTimer.BackColor = System.Drawing.Color.Transparent;
            this.lbSketchTimer.Font = new System.Drawing.Font("BankGothic Lt BT", 20F);
            this.lbSketchTimer.Location = new System.Drawing.Point(81, 702);
            this.lbSketchTimer.Name = "lbSketchTimer";
            this.lbSketchTimer.Size = new System.Drawing.Size(102, 28);
            this.lbSketchTimer.TabIndex = 5;
            this.lbSketchTimer.Text = "00:00";
            // 
            // SketchMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.sketchCanvas);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SketchMaster";
            this.Text = "SketchMaster";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.sketchCanvas.ResumeLayout(false);
            this.sketchCanvas.PerformLayout();
            this.ResumeLayout(false);

        }

        
        private SketchCanvas sketchCanvas;
        private Panel panel1;
        private Panel btSpyOriginal;
        private Panel btSpy;
        private Panel plMarker;
        private Panel plSharpie;
        private Panel plEraser;
        private Panel plPencil;
        private Panel panel8;
        private Panel Darkblue;
        private Panel Black;
        private Panel White;
        private Panel Grey;
        private Panel Yellow;
        private Panel Red;
        private Panel Purple;
        private Panel Green;
        private Panel Lightblue;
        private Panel plBrush;
        private Panel plPen;
        private TextBox txtBoxServerAddress;
        private Button btConnect;
        private Panel plHome;
        private Label lbCountdown;
        private Label lbSketchTimer;


        //PropertyGrid propgrid = new PropertyGrid();
        #endregion
    }
}

