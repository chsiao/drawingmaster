﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Microsoft.Ink;

using SketchGame.Core;
using SketchGame.Core.TestObject;

using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

namespace PBT.ComplexFigure
{
    public partial class FormComplexFigure : Form
    {
        private List<PacketPoint> tmpPacketPoints;
        private List<PenStroke> penStrokes;
        private TestBase testBase;
        private Animation animation;

        private DateTime JanFirst1970 = new DateTime(1970, 1, 1);

        public FormComplexFigure()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.Load += new EventHandler(FormComplexFigure_Load);            
        }

        void FormComplexFigure_Load(object sender, EventArgs e)
        {
            this.inkPicture1.InkEnabled = true;            

            tmpPacketPoints = new List<PacketPoint>();
            penStrokes = new List<PenStroke>();
            testBase = new TestBase();
            testBase.Ink = this.inkPicture1.Ink;
            testBase.PenStrokes = this.penStrokes;

            animation = new Animation(testBase, this.inkPicture1);
        }

        void btExit_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
            Application.Exit();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            FileManage filemanage = new FileManage();
            filemanage.saveFile(this.inkPicture1, "test", this.penStrokes);
        }

        private void inkPicture1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            
        }

        private void inkPicture1_SizeChanged(object sender, System.EventArgs e)
        {
            
        }

        private void inkPicture1_Stroke(object sender, Microsoft.Ink.InkCollectorStrokeEventArgs e)
        {
            if (e.Stroke.PacketDescription.Count() <= 2)
            {
                e.Cancel = true;
                return;
            }

            // Filter out the eraser stroke.
            if (InkOverlayEditingMode.Ink == inkPicture1.EditingMode)
            {
                // Add the new stroke to the ink divider's strokes collection
                //myInkDivider.Strokes.Add(e.Stroke);

                Graphics g = inkPicture1.CreateGraphics(); //try a new kind of graphics creation process
                
                PenStroke penStk = new PenStroke(e.Stroke, g, inkPicture1);
                //this.inkPicture1.Size = new System.Drawing.Size(905, 706);//experiment to play with changing size of window
                penStk.TimeStamp = (long)((DateTime.Now.ToUniversalTime() - JanFirst1970).TotalMilliseconds + 0.5);
                int tempX = penStk.CenterPoint.X;
                int tempY = penStk.CenterPoint.Y;
                //DateTime.Now.ToUniversalTime().Millisecond;//DateTime.Now.Ticks;
                penStk.PacketPoints = this.tmpPacketPoints;

                penStrokes.Add(penStk);
            }

            this.tmpPacketPoints = new List<PacketPoint>();           
        }

        private void inkPicture1_NewPackets(object sender, Microsoft.Ink.InkCollectorNewPacketsEventArgs e)
        {
            PacketPoint pkPoint = new PacketPoint();
            pkPoint.StrokeID = e.Stroke.Id;
            pkPoint.Size = e.Stroke.PacketSize;
            pkPoint.SetTimeLong(DateTime.Now);

            Point temp_pk = new Point((int)e.PacketData.GetValue(0), (int)e.PacketData.GetValue(1));
            Graphics g = inkPicture1.CreateGraphics();
            inkPicture1.Renderer.InkSpaceToPixel(g, ref temp_pk);
            pkPoint.X = temp_pk.X;
            pkPoint.Y = temp_pk.Y;

            #region pressure data
            // trying to get the pressure data from ink collector
            Guid[] PacketDesc = e.Stroke.PacketDescription;

            int NormalPressure = -1;
            for (int k = 0; k < PacketDesc.Length; k++)
            {
                if (PacketDesc[k] == Microsoft.Ink.PacketProperty.NormalPressure)
                {
                    // for simplicity, in case of multiple packets, use the first packet only
                    NormalPressure = e.PacketData[k];
                }
            }

            // If we have the NormalPressure information
            // change DrawingAttributes according to the NormalPressure
            // Note that the change does not take effect until the next stroke
            if (NormalPressure != -1)
            {
                // display the pressure on a status label
                pkPoint.Pressure = NormalPressure;
            }
            #endregion

            tmpPacketPoints.Add(pkPoint);
        }   

        private void chkShowFigure_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkShowFigure.Checked)
                this.inkPicture1.Image = SketchGame.ComplexFigure.Properties.Resources.ComplexFigure;
            else
                this.inkPicture1.Image = null;
        }

        private void btPlay_Click(object sender, EventArgs e)
        {
            Stroke[] strokes = new Stroke[this.inkPicture1.Ink.Strokes.Count];
            
            this.inkPicture1.Ink.Strokes.CopyTo(strokes, 0);

            inkPicture1.InkEnabled = false;
            inkPicture1.Ink = new Microsoft.Ink.Ink();
            inkPicture1.InkEnabled = true;

            this.inkPicture1.Invalidate();
            animation.start(strokes);
        }

        private void btClear_Click(object sender, EventArgs e)
        {
            this.inkPicture1.Ink.DeleteStrokes();
            this.inkPicture1.Ink.Strokes.Clear();
            this.tmpPacketPoints = new List<PacketPoint>();
            this.penStrokes = new List<PenStroke>();

            animation.stop();

            this.inkPicture1.Invalidate();
        }

        private void btScoring_Click(object sender, EventArgs e)
        {
            Image<Gray, byte> template = new Image<Gray, byte>(SketchGame.ComplexFigure.Properties.Resources.ComplexFigure);
            Image<Gray, byte> template2 = new Image<Gray, byte>(SketchGame.ComplexFigure.Properties.Resources.ComplexFigure);

            //Image<Gray,byte> ctlImage = new Image<Gray,byte>(new Bitmap(this.inkPicture1.Image));
            

            Bitmap drawingImage = new Bitmap(this.inkPicture1.Width, this.inkPicture1.Height);
            Graphics g1 = Graphics.FromImage(drawingImage);
            g1.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g1.FillRectangle(Brushes.White, 0, 0, this.inkPicture1.Width, this.inkPicture1.Height);
            // make a renderer to draw ink on the graphics surface
            Renderer r = new Renderer();
            r.Draw(g1, this.inkPicture1.Ink.Strokes);

            Image<Gray, byte> matching = new Image<Gray, byte>(drawingImage);
            Image<Bgr, byte> oriImage = new Image<Bgr, byte>(drawingImage);



            Image<Gray, float> resultImage1 = matching.MatchTemplate(template, TM_TYPE.CV_TM_CCOEFF);
            //Image<Gray, float> resultImage2 = ctlImage.MatchTemplate(template, TM_TYPE.CV_TM_CCOEFF);


            float[,,] matches = resultImage1.Data;
            double maxScore = double.MinValue;
            Point pt = new Point();
            for (int x = 0; x < matches.GetLength(1); x++)
            {
                for (int y = 0; y < matches.GetLength(0); y++)
                {
                    double matchScore = matches[y, x, 0];
                    if (matchScore > 0.75 && matchScore > maxScore)
                    {
                        //Rectangle rect = new Rectangle(new Point(x - template.Width, y - template.Height), new Size(1, 1));
                        pt = new Point(x, y);
                        //template2.Draw(rect, new Gray(1), 1);
                        //ctlImage.Draw(rect, new Gray(1), 1);
                        maxScore = matchScore;
                    }
                }
            }

            Rectangle rect = new Rectangle(pt, new Size(1, 1));
            oriImage.Draw(rect, new Bgr(Color.Blue), 1);
            MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 2, 2);
            double finalScore = Math.Round(((maxScore > 400000000 ? 400000000 : maxScore) / 400000000) * 100, 2);
            oriImage.Draw("Your Score: " + finalScore.ToString(), ref font, new Point(100, 100), new Bgr(Color.Red));
            
            //CvInvoke.cvShowImage("result", oriImage);
            this.lbScore.Text = ": " + finalScore.ToString();

        }
    }
}
