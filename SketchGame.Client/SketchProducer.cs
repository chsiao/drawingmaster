﻿namespace SketchGame.Client
{
    using System;
    using System.Diagnostics;
    using System.Timers;
    using SketchGame.Server;
    using SketchGame.Core;

    public class SketchProducer : ClientBase
    {
        #region Fields

        private static readonly double defaultThrottleTime = 100;

        private Timer timer;

        private bool broadcastAvailable;

        #endregion

        #region Constructors & Destructors

        public SketchProducer()
        {
            this.ThrottleTime = defaultThrottleTime;
            timer = new Timer(this.ThrottleTime);
            timer.Elapsed += this.TimerElapsed;
            this.broadcastAvailable = true;
        }

        #endregion

        #region Properties

        public double ThrottleTime
        {
            get;
            set;
        }

        #endregion

        #region Public Methods

        public void BroadcastStroke(PenStroke stroke)
        {
            if (this.broadcastAvailable)
            {
                this.broadcastAvailable = false;
                ExecuteCommand(this.Client.GetStream(), Command.CreateRequest(CommandNames.StartStroke, stroke));
                this.timer.Start();
            }
            else
            {
                Console.WriteLine("Throttle timer not exceeded, dropping message.");
            }
        }

        public void Connect(string host, int port, AgentCapabilities capability)
        {
            try
            {
                this.Client.Connect(host, port);
                if (!RegisterCapabilities(this.Client, capability))
                {
                    throw new InvalidOperationException("Unable to register capabilities!");
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine("Unable to connect! " + e.ToString());
            }
        }

        #endregion

        #region Private Methods

        private void TimerElapsed(object sender, EventArgs e)
        {
            this.broadcastAvailable = true;
        }

        #endregion
    }
}
