﻿namespace SketchGame.Client
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SketchGame.Server;

    public class CommandEvent : EventArgs
    {
        #region Constructors & Destructors

        public CommandEvent(Command command)
        {
            this.Command = command;
        }

        #endregion

        #region Propeties

        public Command Command
        {
            get;
            private set;
        }

        #endregion
    }
}
