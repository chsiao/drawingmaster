﻿namespace SketchGame.Client
{
    using System;
    using System.Collections.Generic;
    using System.Net.Sockets;
    using System.Threading;
    using SketchGame.Server;
    using SketchGame.Core;
    using SketchGame.Client;

    public class SketchReceiver : ClientBase
    {
        #region Fields

        private Thread pushListener = null;
        private AgentCapabilities Capabilities;

        #endregion

        #region Constructors & Destructors

        public SketchReceiver(AgentCapabilities capability = AgentCapabilities.SingleMode)
            : base()
        {
            this.PushChannel = new TcpClient();
            this.Capabilities = capability;
        }

        #endregion   

        #region Event
        public event EventHandler<CommandEvent> PushNotify;
        #endregion

        #region Properties

        public bool IsConnected
        {
            get
            {
                return (this.Client != null) ? this.Client.Connected : false;
            }
        }

        protected TcpClient PushChannel
        {
            get;
            private set;
        }

        #endregion

        #region Public Methods

        public bool CreateModifyStroke(Command penStrokeCmd)
        {
            if (this.IsConnected)
            {
                Console.WriteLine("Creating Storke Command");
                var stream = this.Client.GetStream();
                ExecuteCommand(stream, penStrokeCmd);

                //var response = ReadResponse(stream);
                //if (response != null && response.Type == CommandTypes.Response)
                //{
                //    return (bool)response.Payload;
                //}
            }

            return false;
        }        

        public void Connect(string host, int port)
        {
            try
            {
                Console.WriteLine("Connecting...");
                this.Client.Connect(host, port);                

                if (!RegisterCapabilities(this.Client, this.Capabilities))
                {
                    throw new InvalidOperationException("Unable to register capabilities!");
                }

                this.PushChannel.Connect(host, port);
                RegisterCapabilities(this.PushChannel, AgentCapabilities.SketchReceive);
                this.pushListener = new Thread(new ThreadStart(this.ListenForPushNotifications));
                this.pushListener.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to connect! " + e.ToString());
                throw;
            }
        }

        #endregion

        #region Private Methods

        private void ListenForPushNotifications()
        {
            while (true)
            {
                var response = ReadResponse(this.PushChannel.GetStream(), true);                
                if (response != null && this.PushNotify != null)
                {
                    //Console.WriteLine("Received push notification! Command: " + response.Name);

                    //switch(response.Name)
                    //{
                    //    case CommandNames.Spying:
                    foreach(Command cmd in response)
                            this.PushNotify(this, new CommandEvent(cmd));
                    //        break;
                    //    case CommandNames.StartSketch:
                    //        this.PushNotify(this, new CommandEvent(response));
                    //        break;
                    //    case CommandNames.SendPacketPoint:
                    //        this.PushNotify(this, new CommandEvent(response));
                    //        break;
                    //    case CommandNames.EndStroke:
                    //        this.PushNotify(this, new CommandEvent(response));
                    //        break;
                    //}
                }
            }
        }

        #endregion
    }
}
