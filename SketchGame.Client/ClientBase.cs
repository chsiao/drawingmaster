﻿namespace SketchGame.Client
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SketchGame.Server;
    using System.Net.Sockets;

    public abstract class ClientBase
    {
        #region Constructors & Destructors

        public ClientBase()
        {
            this.Client = new TcpClient();
        }

        #endregion

        #region Properties

        public TcpClient Client
        {
            get;
            private set;
        }

        #endregion

        #region Protected Methods

        protected static void ExecuteCommand(NetworkStream stream, Command message)
        {
            var bytes = Encoding.ASCII.GetBytes(message.ToJson());
            //Console.WriteLine("Sending " + bytes.Length + " bytes.");
            //Console.WriteLine("Message to send: " + message.ToJson());
            stream.Write(bytes, 0, bytes.Length);
            stream.Flush();
        }

        protected static List<Command> ReadResponse(NetworkStream stream, bool waitForAvailable = false)
        {
            if (stream.CanRead)
            {
                var response = new StringBuilder();
                var buffer = new byte[2048];
                int bytesRead = 0;

                //Console.WriteLine("Waiting on response...");
                //if (!waitForAvailable && stream.DataAvailable)
                //{
                    do
                    {
                        bytesRead = stream.Read(buffer, 0, buffer.Length);
                        //Console.WriteLine("Read " + bytesRead + " bytes");                        
                        string onemessage = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                        response.Append(onemessage);
                        //Console.WriteLine("Message received: " + onemessage);
                    }
                    while (stream.DataAvailable);
                //}

                    object cmdObject = null;//response.Length > 0 ? response.ToString().FromJsonToType<Command>() : null;

                if (cmdObject == null)
                {
                    cmdObject = string.IsNullOrEmpty(response.ToString()) ? null : response.ToString().FromJsonToTypeList<Command>();
                    if (cmdObject != null)
                    {
                        string leftover = ((Tuple<List<Command>, string>)cmdObject).Item2;
                        cmdObject = ((Tuple<List<Command>, string>)cmdObject).Item1;
                    }
                }

                List<Command> returnObj = new List<Command>();
                if (cmdObject != null)
                {
                    if (cmdObject is Command)
                        returnObj.Add((Command)cmdObject);
                    else if (cmdObject is List<Command>)
                        returnObj.AddRange((List<Command>)cmdObject);
                }

                return returnObj;
            }
            else
            {
                Console.WriteLine("Unable to read from the network stream! Possible connection error.");
                return null;
            }
        }

        protected static bool RegisterCapabilities(TcpClient channelToAssign, AgentCapabilities capability)
        {
            var stream = channelToAssign.GetStream();
            ExecuteCommand(stream, Command.CreateRequest(CommandNames.Register, capability));

            var response = ReadResponse(stream).First();
            if (response != null && response.Type == CommandTypes.Response)
            {
                return (bool)response.Payload;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
